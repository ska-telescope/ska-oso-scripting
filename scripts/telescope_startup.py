import functools
import logging
import os
import time

from tango import DeviceProxy, DevState, Database
from ska_oso_scripting.event import user_topics
from ska_oso_scripting.functions import messages
from ska_oso_scripting.functions import environment
from ska_oso_scripting.objects import Telescope

LOG = logging.getLogger(__name__)
FORMAT = "%(asctime)-15s %(message)s"

logging.basicConfig(level=logging.INFO, format=FORMAT)


def init(subarray_id: int):
    """
    Initialise the script, binding the sub-array ID to the script.
    """
    LOG.debug(f"Initializing script {__name__} with subarray_id={subarray_id}")
    global main
    main = functools.partial(_main, subarray_id)
    LOG.info(f"Script bound to sub-array {subarray_id}")


def turn_low_on():
    """
    Script to configure and turn on a SKA-LOW software telescope before
    executing an SBD using the OET.
    """
    # Create telescope instance
    tel = Telescope()
    
    # Put CSP into a state such that it can receive commands
    csp_controller = DeviceProxy("low-csp/control/0")
    csp_controller.write_attribute("adminMode", 0)
    time.sleep(5)
    msg = f"CSP Controller State: {csp_controller.State()}"
    LOG.info(msg)
    messages.send_message(user_topics.script.announce, msg=msg)
    
    # Create TMC Central Node device proxy
    cn_proxy = DeviceProxy("ska_low/tm_central/central_node")
    cn_proxy.ping()

    # Turn on MCCS devices
    db = Database()
    low_mccs_device_strings = db.get_device_exported("low-mccs/*")
    low_mccs_devices = []
    for device_str in low_mccs_device_strings:
        mccs_device = DeviceProxy(device_str)
        mccs_device.adminMode = 0
        low_mccs_devices.append(mccs_device)

    # Turn CSP on
    csp_controller.On([])
    time.sleep(2)
    msg = f"CSP Controller State: {csp_controller.State()}"
    LOG.info(msg)
    messages.send_message(user_topics.script.announce, msg=msg)

    # Turn the telescope on!
    if int(cn_proxy.telescopeState) in [1, 7]:
        msg = "Telescope is either in OFF or STANDBY state. Turning telescope ON."
        LOG.info(msg)
        messages.send_message(user_topics.script.announce, msg=msg)
        tel.on()
    else:
        msg = f"WARNING: Telescope is in state ({list(DevState.names.keys())[list(DevState.names.values()).index(cn_proxy.telescopeState)]}), will not send On command."
        LOG.info(msg)
        messages.send_message(user_topics.script.announce, msg=msg)


def turn_mid_on():
    """
    Script to configure and turn on a SKA-MID software telescope before
    executing an SBD using the OET.
    """
    # Create telescope instance
    tel = Telescope()

    csp_controller = DeviceProxy("mid-csp/control/0")
    central_node = DeviceProxy('ska_mid/tm_central/central_node')

    if csp_controller.adminMode.value == 1:
        csp_controller.write_attribute("adminMode", 0)
        timeout = 10
        while str(csp_controller.State()) != "OFF" or central_node.isDishVccConfigSet == False:
            time.sleep(1)
            timeout -= 1
            if timeout == 0:
                msg = f"WARNING: Timeout while waiting for CSP Controller to go from DISABLE to OFF or isDishVccConfigSet on CentralNode to turn True"
                LOG.info(msg)
                messages.send_message(user_topics.script.announce, msg=msg)
                break

    # CSP Controller State should be OFF at this point instead of DISABLE
    msg = f"CSP Controller State: {csp_controller.State()}"
    LOG.info(msg)
    messages.send_message(user_topics.script.announce, msg=msg)
    # CentralNode's isDishVccConfigSet should be True once CSP Controller is ONLINE and OFF
    msg = f"CentralNode isDishVccConfigSet: {central_node.isDishVccConfigSet}"
    LOG.info(msg)
    messages.send_message(user_topics.script.announce, msg=msg)

    # Turn the telescope on!
    if int(central_node.telescopeState) in [1, 7]:
        msg = "Telescope is either in OFF or STANDBY state. Turning telescope ON."
        LOG.info(msg)
        messages.send_message(user_topics.script.announce, msg=msg)
        tel.on()
    else:
        msg = f"WARNING: Telescope is in state ({list(DevState.names.keys())[list(DevState.names.values()).index(central_node.telescopeState)]}), will not send On command."
        LOG.info(msg)
        messages.send_message(user_topics.script.announce, msg=msg)

        
def _main(subarray_id: int):
    msg = f"Running OS process {os.getpid()}"
    LOG.info(msg)
    messages.send_message(user_topics.script.announce, msg=msg)

    if environment.is_ska_low_environment():
        turn_low_on()
    else:
        turn_mid_on()
