"""
The ska_oso_scripting.functions.messages module contains functions that scripts can use
to announce events and messages to the outside world.
"""
import threading

from pubsub import pub

from ska_oso_scripting.event import user_topics


def send_message(topic, **kwargs):
    """
    Helper function to send messages via pypubsub.

    :param topic: topic matching a topic in oet.event.topics
    :param kwargs: kwargs to be included in message
    """
    pub.sendMessage(topic, msg_src=threading.current_thread().name, **kwargs)


def publish_event_message(topic=user_topics.script.announce, **kwargs):
    """
    publish pypubsub event messages, OET scripts will be using this method
    to publish a freeform messages to an unknown listener.

    :param topic: message topic
    :param kwargs: any metadata associated with pypubsub message
    """
    # TODO:the custom topic should be a unique one, compare it with oet.event.topics
    send_message(topic, **kwargs)
