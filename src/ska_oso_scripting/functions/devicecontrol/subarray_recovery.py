from ska_tango_base.control_model import ObsState

from ska_oso_scripting.functions.devicecontrol.common import (
    TANGO_REGISTRY,
    _call_and_wait_for_transition,
)
from ska_oso_scripting.functions.devicecontrol.tango_executor import Command
from ska_oso_scripting.types import SubarrayID


def abort(subarray_id: SubarrayID):
    """
    Send the 'abort' command to the SubArrayNode, halt the subarray
    activity.

    :param subarray_id: the subarray to command
    """

    command = get_abort_command(subarray_id)
    _call_and_wait_for_transition(
        command, [ObsState.ABORTING, ObsState.ABORTED], "obsState"
    )


def get_abort_command(subarray_id: SubarrayID) -> Command:
    """
    Return an OET Command that, when passed to a TangoExecutor, would call
    SubArrayNode.Abort().

    :param subarray_id: the SubArray to control
    :return: the OET Command
    """
    subarray_node_fqdn = TANGO_REGISTRY.get_subarray_node(subarray_id)
    return Command(subarray_node_fqdn, "Abort")


def obsreset(subarray_id: SubarrayID):
    """
    Send the 'ObsReset' command to the SubArrayNode, which resets
    the SubArrayNode state to IDLE.

    :param subarray_id: the subarray to command
    """
    command = get_obsreset_command(subarray_id)
    _call_and_wait_for_transition(
        command, [ObsState.RESETTING, ObsState.IDLE], "obsState"
    )


def get_obsreset_command(subarray_id: SubarrayID) -> Command:
    """
    Return an OET Command that, when passed to a TangoExecutor, would call
    SubArrayNode.ObsReset().

    :param subarray_id: the SubArray to control
    :return: the OET Command
    """
    subarray_node_fqdn = TANGO_REGISTRY.get_subarray_node(subarray_id)
    return Command(subarray_node_fqdn, "ObsReset")


def restart(subarray_id: SubarrayID):
    """
    Send the 'restart' command to the SubArrayNode which sets
    the SubArrayNode from ABORTED or FAULT state to EMPTY.

    :param subarray_id: the subarray to command
    """

    command = get_restart_command(subarray_id)
    _call_and_wait_for_transition(
        command, [ObsState.RESTARTING, ObsState.EMPTY], "obsState"
    )


def get_restart_command(subarray_id: SubarrayID) -> Command:
    """
    Return an OET Command that, when passed to a TangoExecutor, would call
    SubArrayNode.Restart().

    :param subarray_id: the SubArray to control
    :return: the OET Command
    """
    subarray_node_fqdn = TANGO_REGISTRY.get_subarray_node(subarray_id)
    return Command(subarray_node_fqdn, "Restart")
