.. _script-examples:

***************************
Writing scripts for the OET
***************************

The Observation Execution Tool (OET) can run observing scripts in a headless
non-interactive manner. For efficiency, OET script execution is split into two phases:
an initialisation phase and an execution phase. Scripts that are expected to be run by
the OET should be structured to have two entry points corresponding to these two
phases, as the template below:

.. code-block:: python
   :caption: Observing script template
   :linenos:

   def init(subarray: int, *args, **kwargs):
       # Called by the OET when the script is loaded and initialised by someone
       # calling 'oet prepare'. Add your script initialisation code here. Note that
       # the target subarray is supplied to this function as the first argument.
       pass

   def main(*args, **kwargs):
       # Called by the OET when the prepared script is told to run by someone
       # calling 'oet start'. Add the main body of your script to this function.
       pass

The initialisation phase occurs when the script is loaded and the script's
``init`` function is called (if defined) to perform any preparation and/or
initialisation. Expensive and slow operations that can be performed ahead of the main
body of script execution can be run in the initialisation phase. Typical actions
performed in init are I/O intensive operations, e.g., cloning a git repository,
creating multiple Tango device proxies, subscribing to Tango events, etc. When run by
the Observation Execution Tool (OET), the ``init`` function is passed an integer
subarray ID declaring which subarray the control script is intended to control.

Subsequently, at some point a user may call ``oet start``, requesting that the
initialised script begin the main body of its execution. When this occurs, the OET
calls the script's ``main`` function, which should performs the main function of the
script. For an observing script, this would involve the configuration and control of a
subarray.

below is the real example script in the ``scripts`` folder of this project.


----------------------------------------------------------------------------
SKA : Allocate Resources and Perform Observation 
----------------------------------------------------------------------------
Allocating resources and performing scans requires communication with TMC CentralNode and
TMC SubarrayNode, and targets a specific subarray. This script's ``init`` function
pre-applies the subarray ID argument to the main function. Note that this script does not
perform any Tango calls directly, but uses ``ska_oso_scripting.functions.devicecontrol``
functions to perform all the required Tango interactions (command invocation; event
subscriptions; event monitoring).

.. literalinclude:: ../../scripts/allocate_and_observe_sb.py
   :language: python
   :caption: Resource allocation and perform observation script for an SKA MID/LOW subarray
   :linenos:
