.. _devicecontrol-guide:


*****************************************
ska_oso_scripting.functions.devicecontrol
*****************************************

.. automodule:: ska_oso_scripting.functions.devicecontrol
    :members: assign_resources_from_file, assign_resources_from_cdm,
            assign_resources_from_json, release_all_resources,
            release_resources, configure_from_file, configure_from_cdm,
            scan, end, telescope_on, telescope_off, abort, obsreset, restart
