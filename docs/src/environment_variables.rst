.. _environment_variables:

*********************
Environment Variables
*********************

Environment variables are used by ska-oso-scripting to define the execution environment
and which Tango devices should be controlled. This in turn modifies the behaviour of
the code, giving different behaviour depending on whether the code is running in an
SKA MID environment (default) or an SKA LOW environment. For example, when configured
for SKA MID, connections to a subarray will connect to an SKA MID subarray, and
validation code will reject CDM payloads intended for SKA LOW.

.. list-table:: Environment variables recognised by ska-oso-scripting
   :widths: 25 25 50
   :header-rows: 1

   * - Variable
     - Default value
     - Description
   * - ``SKA_TELESCOPE``
     - ``skamid``
     - Controls the behaviour of telescope-specific functions to expect SKA LOW
       (``skalow``) or SKA MID (``skamid``). If the environment variable is not
       recognised, ``skamid`` will be assumed.
   * - ``CENTRALNODE_FQDN``
     - ``ska_mid/tm_central/central_node``
     - The fully-qualified domain name (FQDN) of the TMC CentralNode Tango device. If
       left unset, an appropriate FQDN for SKA MID will be used.
   * - ``SUBARRAYNODE_FQDN_PREFIX``
     - ``ska_mid/tm_subarray_node``
     - Prefix to use when constructing the FQDN for a TMC SubarrayNode Tango device.
       If left unset, an appropriate FQDN prefix for SKA MID will be used.
   * - ``ODA_URL``
     - ``null``
     - The base API location for an instance of the ODA, to be used to create and update Execution Blocks.
       For example, https://k8s.stfc.skao.int/button-dev-ska-db-oda/oda/api/v1/
