import logging
from pathlib import Path

import pytest
from ska_oso_pdm import SBDefinition
from ska_tmc_cdm.messages.central_node.assign_resources import AssignResourcesRequest
from ska_tmc_cdm.messages.subarray_node.configure import ConfigureRequest
from ska_tmc_cdm.schemas import CODEC

from ska_oso_scripting.functions import sb

LOGGER = logging.getLogger(__name__)

TEST_RESOURCES = Path(__file__).parents[2] / "resources"


@pytest.fixture(autouse=False)
def override_tmdata(monkeypatch):
    """
    Set Telescope Model static data source for all integration tests.

    This fixture can be used to point TelModel at an older version of the
    accompanying Telescope Model static data (TMData) in cases where the
    current version of TMData cannot be found or is corrupt.
    """
    # Set to the version for TMData to use instead of current TelModel version
    tmdata_ver = "1.18.0"
    LOGGER.warning("Overriding TelModel TMData version: using %s", tmdata_ver)
    monkeypatch.setenv(
        "SKA_TELMODEL_SOURCES",
        f"car://gitlab.com/ska-telescope/ska-telmodel?{tmdata_ver}#tmdata",
    )


@pytest.fixture
def mid_allocation_request_json_path() -> Path:
    """
    Pytest fixture to return path to resource allocation JSON file
    for SKA-mid
    """
    json_path = (
        TEST_RESOURCES
        / "pdm_transforms_test_cases/mid_2single_pointing_no_sdp/cdm_assign_resources.json"
    )
    return json_path


@pytest.fixture
def mid_allocation_invalid_json_path_semantic_validation() -> Path:
    json_path = (
        TEST_RESOURCES
        / "semantic_validation_test_cases/testfile_invalid_assign_semantic_validate.json"
    )
    return json_path


@pytest.fixture
def low_allocation_request_json_path() -> Path:
    """
    Pytest fixture to return resource allocation JSON for SKA-low
    """
    return (
        TEST_RESOURCES
        / "pdm_transforms_test_cases/low_no_tied_array_no_sdp/cdm_assign_resources.json"
    )


@pytest.fixture
def low_allocation_invalid_json_path_semantic_validation() -> Path:
    json_path = (
        TEST_RESOURCES
        / "semantic_validation_test_cases/testfile_invalid_low_assign_semantic_validate.json"
    )
    return json_path


@pytest.fixture
def mid_allocation_request_cdm(
    mid_allocation_request_json_path,
) -> AssignResourcesRequest:
    """
    Pytest fixture to return AssignResourcesRequest CDM object for SKA-mid
    """
    return CODEC.load_from_file(
        AssignResourcesRequest, mid_allocation_request_json_path
    )


@pytest.fixture
def mid_allocation_invalid_cdm_semantic_validation(
    mid_allocation_invalid_json_path_semantic_validation,
) -> AssignResourcesRequest:
    return CODEC.load_from_file(
        AssignResourcesRequest,
        mid_allocation_invalid_json_path_semantic_validation,
        strictness=2,
    )


@pytest.fixture
def low_allocation_request_cdm(
    low_allocation_request_json_path,
) -> AssignResourcesRequest:
    """
    Pytest fixture to return AssignResourcesRequest CDM object for SKA-low
    """
    return CODEC.load_from_file(
        AssignResourcesRequest, low_allocation_request_json_path
    )


@pytest.fixture
def low_allocation_invalid_cdm_semantic_validation(
    low_allocation_invalid_json_path_semantic_validation,
) -> AssignResourcesRequest:
    return CODEC.load_from_file(
        AssignResourcesRequest,
        low_allocation_invalid_json_path_semantic_validation,
        strictness=2,
    )


@pytest.fixture()
def mid_allocation_request_json(mid_allocation_request_cdm) -> str:
    return CODEC.dumps(mid_allocation_request_cdm)


@pytest.fixture()
def mid_allocation_invalid_json_semantic_validation(
    mid_allocation_invalid_cdm_semantic_validation,
) -> str:
    return CODEC.dumps(mid_allocation_invalid_cdm_semantic_validation)


@pytest.fixture()
def low_allocation_request_json(low_allocation_request_cdm) -> str:
    return CODEC.dumps(low_allocation_request_cdm)


@pytest.fixture()
def low_allocation_invalid_json_semantic_validation(
    low_allocation_invalid_cdm_semantic_validation,
) -> str:
    return CODEC.dumps(low_allocation_invalid_cdm_semantic_validation)


@pytest.fixture
def mid_configure_request_json_path():
    """
    Pytest fixture to return path to configuration JSON file
    for SKA-mid
    """
    return (
        TEST_RESOURCES
        / "pdm_transforms_test_cases/mid_2single_pointing_no_sdp/cdm_configure.json"
    )


@pytest.fixture
def low_configure_request_json_path():
    """
    Pytest fixture to return configure JSON for SKA-low
    """
    return (
        TEST_RESOURCES
        / "pdm_transforms_test_cases/low_no_tied_array_no_sdp/cdm_configure.json"
    )


@pytest.fixture
def path_to_semantically_invalid_configurerequest_json_for_mid() -> Path:
    """
    Pytest fixture providing a path to a file containing semantically invalid
    JSON for an SKA MID configure request.
    """
    return (
        TEST_RESOURCES
        / "semantic_validation_test_cases/testfile_invalid_configure_semantic_validate.json"
    )


@pytest.fixture
def path_to_semantically_invalid_configurerequest_json_for_low() -> Path:
    """
    Pytest fixture providing a path to a file containing semantically invalid
    JSON for an SKA LOW configure request.
    """
    return (
        TEST_RESOURCES
        / "semantic_validation_test_cases/testfile_invalid_low_configure_semantic_validate.json"
    )


@pytest.fixture
def mid_configure_request_cdm(mid_configure_request_json_path):
    """
    Pytest fixture to return ConfigureRequest CDM object for SKA-mid
    """
    return CODEC.load_from_file(ConfigureRequest, mid_configure_request_json_path)


@pytest.fixture
def semantically_invalid_mid_configurerequest(
    path_to_semantically_invalid_configurerequest_json_for_mid,
) -> ConfigureRequest:
    """
    Test fixture to provide a semantically invalid SKA LOW ConfigureRequest
    for SKA MID.
    """
    return CODEC.load_from_file(
        ConfigureRequest, path_to_semantically_invalid_configurerequest_json_for_mid
    )


@pytest.fixture
def low_configure_request_cdm(low_configure_request_json_path):
    """
    Test fixture to return a valid ConfigureRequest CDM object for SKA LOW.
    """
    return CODEC.load_from_file(ConfigureRequest, low_configure_request_json_path)


@pytest.fixture
def semantically_invalid_low_configurerequest(
    path_to_semantically_invalid_configurerequest_json_for_low,
) -> ConfigureRequest:
    """
    Test fixture to provide a semantically invalid ConfigureRequest CDM
    instance for SKA LOW.
    """
    return CODEC.load_from_file(
        ConfigureRequest, path_to_semantically_invalid_configurerequest_json_for_low
    )


@pytest.fixture()
def mid_configure_request_json(mid_configure_request_cdm) -> str:
    """
    Test fixture to provide a valid ConfigureRequest JSON for SKA MID.
    """
    return CODEC.dumps(mid_configure_request_cdm)


@pytest.fixture()
def low_configure_request_json(low_configure_request_cdm) -> str:
    """
    Test fixture to provide JSON for a valid SKA LOW configure request.
    """
    return CODEC.dumps(low_configure_request_cdm)


@pytest.fixture
def semantically_invalid_mid_sbd() -> SBDefinition:
    """
    Test fixture to provide a semantically invalid MID SB Definition.
    """
    json_path = TEST_RESOURCES / "integration_test_cases/testfile_invalid_sb_mid.json"
    return sb.load_sbd(json_path)
