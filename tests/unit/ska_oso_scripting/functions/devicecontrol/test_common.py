"""
Unit tests for the ska_oso_scripting.functions.devicecontrol.common module
"""
import copy
import json
import os
import re
import unittest.mock as mock

import pytest
import tango
from ska_ost_osd.telvalidation.schematic_validation_exceptions import (
    SchematicValidationError,
)
from ska_tango_base.control_model import ObsState
from ska_tmc_cdm.messages.central_node.assign_resources import AssignResourcesRequest
from ska_tmc_cdm.messages.subarray_node.configure import ConfigureRequest
from ska_tmc_cdm.schemas import CODEC

from ska_oso_scripting.functions.devicecontrol import common
from ska_oso_scripting.functions.devicecontrol.exception import EventTimeoutError

from ... import utils


def create_event(value, err: bool):
    """
    Create event with given value and error state
    """
    evt = mock.MagicMock(spec_set=tango.EventData)
    devattribute = mock.MagicMock(spec=tango.DeviceAttribute)
    devattribute.value = value
    evt.attr_value = devattribute
    evt.err = err
    return evt


def test_tango_registry_returns_default_url_for_ska_mid_central_node():
    """
    TangoRegistry should return URL for SKA Mid Central Node by default
    when CENTRALNODE_FQDN environment variable is not set.
    """
    registry = common.TangoRegistry()
    fqdn = registry.get_central_node()
    assert fqdn == utils.SKA_MID_CENTRAL_NODE_FQDN


@mock.patch.dict(os.environ, {"SKA_TELESCOPE": "SKA-Low"})
def test_tango_registry_returns_default_url_for_ska_low_central_node():
    """
    TangoRegistry should return URL for SKA Low Central Node by default
    when CENTRALNODE_FQDN environment variable is not set and SKA_TELESCOPE
    is set to SKA-Low.
    """
    registry = common.TangoRegistry()
    fqdn = registry.get_central_node()
    assert fqdn == utils.SKA_LOW_CENTRAL_NODE_FQDN


@mock.patch.dict(os.environ, {"CENTRALNODE_FQDN": "ska_low/tm_central/central_node"})
def test_tango_registry_returns_correct_url_for_ska_low_central_node():
    """
    TangoRegistry should return the Central Node URL set in the
    environment variable.
    """
    registry = common.TangoRegistry()
    fqdn = registry.get_central_node()
    assert fqdn == utils.SKA_LOW_CENTRAL_NODE_FQDN


def test_tango_registry_returns_default_url_for_ska_mid_subarray():
    """
    TangoRegistry should return URL for SKA Mid Subarray Node by default
    when SUBARRAYNODE_FQDN environment variable is not set.
    """
    registry = common.TangoRegistry()
    fqdn = registry.get_subarray_node(1)
    assert fqdn == utils.SKA_MID_SUBARRAY_NODE_1_FQDN


@mock.patch.dict(os.environ, {"SKA_TELESCOPE": "SKA-Low"})
def test_tango_registry_returns_default_url_for_ska_low_subarray():
    """
    TangoRegistry should return URL for SKA Low Subarray Node by default
    when SUBARRAYNODE_FQDN environment variable is not set and SKA_TELESCOPE
    is set to SKA-Low.
    """
    registry = common.TangoRegistry()
    fqdn = registry.get_subarray_node(1)
    assert fqdn == utils.SKA_LOW_SUBARRAY_NODE_1_FQDN


@mock.patch.dict(os.environ, {"SUBARRAYNODE_FQDN_PREFIX": "ska_low/tm_subarray_node"})
def test_tango_registry_returns_correct_url_for_ska_low_subarray():
    """
    TangoRegistry should return the Subarray Node URL set in the
    environment variable.
    """
    registry = common.TangoRegistry()
    fqdn = registry.get_subarray_node(1)
    assert fqdn == utils.SKA_LOW_SUBARRAY_NODE_1_FQDN


@pytest.mark.parametrize(
    "published,target,msg",
    [
        (
            ObsState.READY,
            ObsState.READY,
            common.WAIT_FOR_TRANSITION_SUCCESS_RESPONSE,
        ),
        (
            ObsState.FAULT,
            ObsState.READY,
            common.WAIT_FOR_TRANSITION_FAILURE_RESPONSE,
        ),
        (
            tango.DevState.ON,
            tango.DevState.ON,
            common.WAIT_FOR_TRANSITION_SUCCESS_RESPONSE,
        ),
        (
            tango.DevState.OFF,
            tango.DevState.ON,
            common.WAIT_FOR_TRANSITION_FAILURE_RESPONSE,
        ),
    ],
)
def test_wait_for_transition(published, target, msg):
    """
    Test that wait_for_transition waits for the next event, returning a success
    or failure response depending on whether the received event matches that
    expected.
    """
    attribute = common.Attribute(utils.SKA_MID_SUBARRAY_NODE_1_FQDN, "attribute")
    event_sequence = [create_event(published, False)]

    with mock.patch(
        "ska_oso_scripting.functions.devicecontrol.common.EXECUTOR.read_event"
    ) as mock_fn:
        mock_fn.side_effect = event_sequence
        state_response = common.wait_for_transition(attribute, target)
    assert state_response.response_msg == msg
    assert state_response.final_value == published


@pytest.mark.parametrize(
    "published,optional,target,msg",
    [
        (
            ObsState.READY,
            ObsState.IDLE,
            ObsState.READY,
            common.WAIT_FOR_TRANSITION_SUCCESS_RESPONSE,
        ),
        (
            ObsState.FAULT,
            ObsState.IDLE,
            ObsState.READY,
            common.WAIT_FOR_TRANSITION_FAILURE_RESPONSE,
        ),
        (
            tango.DevState.ON,
            tango.DevState.UNKNOWN,
            tango.DevState.ON,
            common.WAIT_FOR_TRANSITION_SUCCESS_RESPONSE,
        ),
        (
            tango.DevState.OFF,
            tango.DevState.UNKNOWN,
            tango.DevState.ON,
            common.WAIT_FOR_TRANSITION_FAILURE_RESPONSE,
        ),
    ],
)
def test_wait_for_transition_with_optional_transitions(
    published, optional, target, msg
):
    """
    Test that wait_for_transition waits for the next event, returning a success
    or failure response depending on whether the received event matches that
    expected.
    """
    attribute = common.Attribute(utils.SKA_MID_SUBARRAY_NODE_1_FQDN, "attribute")
    event_sequence = [
        create_event(optional, False),
        create_event(published, False),
    ]

    with mock.patch(
        "ska_oso_scripting.functions.devicecontrol.common.EXECUTOR.read_event"
    ) as mock_fn:
        mock_fn.side_effect = event_sequence
        state_response = common.wait_for_transition(
            attribute, target, optional_transitions=[optional]
        )
    assert state_response.final_value == published
    assert state_response.response_msg == msg


# def test_wait_for_pubsub_value_raises_exception_on_timeout():
#     """
#     Verify wait_for_pubsub_value raises exception if timeout occurs
#     """
#     with common.EXECUTOR.queue.mutex:
#         common.EXECUTOR.queue.queue.clear()
#     target_states = [ObsState.ABORTED, ObsState.FAULT, ObsState.IDLE]
#
#     with pytest.raises(Empty):
#         _ = common.wait_for_pubsub_value(target_states,
#                                                  key=common.
#                                                  parse_oet_obsstate_from_tango_eventdata,
#                                                  timeout=1)


def test_wait_for_value_raises_exception_on_event_error():
    """
    Verify wait_for_value raises exception if event with an error is encountered
    """
    evt = mock.MagicMock(spec_set=tango.EventData)
    evt.attr_value = None
    evt.err = True
    evt.errors = [mock.MagicMock(spec_set=tango.DevError)]

    attribute = common.Attribute(utils.SKA_MID_CENTRAL_NODE_FQDN, "telescopeState")
    target_states = [ObsState.ABORTED, ObsState.FAULT, ObsState.IDLE]

    with mock.patch(
        "ska_oso_scripting.functions.devicecontrol.common.EXECUTOR.read_event"
    ) as mock_fn:
        mock_fn.side_effect = [evt]
        with pytest.raises(Exception):
            _ = common.wait_for_value(attribute, target_states)


def test_wait_for_value_raises_type_error_for_non_matching_types_in_pubsub():
    """
    Verify wait_for_value raises TypeError if attribute type and
    target type do not match
    """
    event_with_bad_value_type = create_event("Hello World", False)

    attribute = common.Attribute(utils.SKA_MID_CENTRAL_NODE_FQDN, "telescopeState")
    target_states = [ObsState.ABORTED, ObsState.FAULT, ObsState.IDLE]

    with mock.patch(
        "ska_oso_scripting.functions.devicecontrol.common.EXECUTOR.read_event"
    ) as mock_fn:
        mock_fn.side_effect = [event_with_bad_value_type]
        with pytest.raises(TypeError):
            _ = common.wait_for_value(
                attribute,
                target_states,
                key=common.parse_oet_obsstate_from_tango_eventdata,
            )


@mock.patch.object(common.EXECUTOR, "execute")
@mock.patch.object(common.EXECUTOR, "subscribe_event")
@mock.patch.object(common.EXECUTOR, "read_event")
@mock.patch.object(common.EXECUTOR, "unsubscribe_event")
def test_call_and_wait_for_transition_waits_for_target_states(
    mock_subscribe_event_fn,
    mock_read_event_fn,
    mock_unsubscribe_event_fn,
    mock_execute_fn,
):
    """
    Test that the call_and_wait_for_attribute_value function waits for the requested
    states in the specified sequence for pub/sub feature.
    """
    mock_read_event_fn.side_effect = [
        create_event(evt, False) for evt in [ObsState.RESOURCING, ObsState.IDLE]
    ]

    mock_execute_fn.return_value = utils.MID_CN_ASSIGN_RESOURCES_SUCCESS_RESPONSE

    # Test command to call SubArrayNode.Foo()
    cmd = common.Command(utils.SKA_MID_SUBARRAY_NODE_1_FQDN, "Foo")

    # This task waits for, in sequence, RESOURCING then IDLE.
    _ = common._call_and_wait_for_transition(
        cmd, [ObsState.RESOURCING, ObsState.IDLE], "obsState"
    )

    mock_subscribe_event_fn.assert_called_once()
    mock_unsubscribe_event_fn.assert_called_once()
    # SubArrayNode.Foo() should just have been called once
    mock_execute_fn.assert_called_once()


@mock.patch.object(common.EXECUTOR, "execute")
@mock.patch.object(common.EXECUTOR, "subscribe_event")
@mock.patch.object(common.EXECUTOR, "read_event")
@mock.patch.object(common.EXECUTOR, "unsubscribe_event")
def test_call_and_wait_for_transition_raises_exception_when_error_state_encountered(
    mock_subscribe_event_fn,
    mock_read_event_fn,
    mock_unsubscribe_event_fn,
    mock_execute_fn,
):
    """
    Verify that call_and_wait_for_attribute_value raises an exception when an error
    state is encountered for pub/sub feature.
    """
    # obsState will be SCANNING for the first three reads, then FAULT
    mock_read_event_fn.side_effect = [create_event(ObsState.FAULT, False)]

    # Test command to call SubArrayNode.Foo()
    cmd = common.Command(utils.SKA_MID_SUBARRAY_NODE_1_FQDN, "Foo")

    with pytest.raises(common.ValueTransitionError):
        common._call_and_wait_for_transition(cmd, [ObsState.READY], "obsState")

    mock_subscribe_event_fn.assert_called_once()
    mock_unsubscribe_event_fn.assert_called_once()
    mock_execute_fn.assert_called_once()


def test_call_and_wait_for_transition_raise_exception_if_subscribe_fails():
    """
    Verify that call_and_wait_for_attribute_value raise exception if subscribing
    to a device gives DevFailed error.
    """
    cmd = common.Command(utils.SKA_MID_SUBARRAY_NODE_1_FQDN, "Foo")
    with pytest.raises(tango.DevFailed):
        common._call_and_wait_for_transition(cmd, [ObsState.READY], "obsState")


@mock.patch.object(common.EXECUTOR, "execute")
@mock.patch.object(common.EXECUTOR, "subscribe_event")
@mock.patch.object(common.EXECUTOR, "read_event")
@mock.patch.object(common.EXECUTOR, "unsubscribe_event")
@mock.patch.object(common, "transaction")
def test_call_and_wait_for_transition_adds_transaction_id_and_log_for_assign_resource_command(
    mock_txn_id_fn,
    mock_unsubscribe_event_fn,
    mock_read_event_fn,
    mock_subscribe_event_fn,
    mock_execute_fn,
    mid_allocation_request_cdm,
    caplog,
):
    """
    Test that the _call_and_wait_for_transition function adds transaction_id to the
    request payload of command and also generates transaction_id logs required.
    """
    mock_txn_id_fn.return_value.__enter__.return_value = "txn-local-20220802-251597784"
    mock_read_event_fn.side_effect = [
        create_event(evt, False) for evt in [ObsState.RESOURCING, ObsState.IDLE]
    ]

    mock_execute_fn.return_value = utils.MID_CN_ASSIGN_RESOURCES_SUCCESS_RESPONSE

    # creating command
    request = CODEC.dumps(mid_allocation_request_cdm)
    command = common.Command(
        utils.SKA_MID_CENTRAL_NODE_FQDN, "AssignResources", request
    )

    _ = common._call_and_wait_for_transition(
        command, [ObsState.RESOURCING, ObsState.IDLE], "obsState"
    )

    assert """ "transaction_id": "txn-local-20220802-251597784""" in caplog.text

    mock_subscribe_event_fn.assert_called_once()
    mock_unsubscribe_event_fn.assert_called_once()
    mock_execute_fn.assert_called_once()


@mock.patch.object(common.EXECUTOR, "execute")
@mock.patch.object(common.EXECUTOR, "subscribe_event")
@mock.patch.object(common.EXECUTOR, "read_event")
@mock.patch.object(common.EXECUTOR, "unsubscribe_event")
@mock.patch.object(common, "transaction")
def test_call_and_wait_for_transition_adds_transaction_id_and_log_for_configure_command(
    mock_txn_id_fn,
    mock_unsubscribe_event_fn,
    mock_read_event_fn,
    mock_subscribe_event_fn,
    mock_execute_fn,
    mid_configure_request_cdm,
    caplog,
):
    """
    Test that the call_and_wait_for_attribute_value function waits for the requested
    states in the specified sequence for pub/sub feature.
    """
    mock_txn_id_fn.return_value.__enter__.return_value = "txn-local-20220802-251597784"
    mock_read_event_fn.side_effect = [
        create_event(evt, False) for evt in [ObsState.CONFIGURING, ObsState.READY]
    ]

    # creating command
    request = CODEC.dumps(mid_configure_request_cdm)
    command = common.Command(utils.SKA_MID_SUBARRAY_NODE_1_FQDN, "Configure", request)

    _ = common._call_and_wait_for_transition(
        command, [ObsState.CONFIGURING, ObsState.READY], "obsState"
    )

    assert """ "transaction_id": "txn-local-""" in caplog.text

    mock_subscribe_event_fn.assert_called_once()
    mock_unsubscribe_event_fn.assert_called_once()
    mock_execute_fn.assert_called_once()


@mock.patch.object(common.EXECUTOR, "execute")
@mock.patch.object(common.EXECUTOR, "subscribe_event")
@mock.patch.object(common.EXECUTOR, "read_event")
@mock.patch.object(common.EXECUTOR, "unsubscribe_event")
@mock.patch.object(common, "transaction")
def test_transaction_id_gets_replaced_when_present_in_request_payload(
    mock_txn_id_fn,
    mock_unsubscribe_event_fn,
    mock_read_event_fn,
    mock_subscribe_event_fn,
    mock_execute_fn,
    mid_allocation_request_cdm,
    caplog,
):
    """
    Test that the call_and_wait_for_attribute_value function replace old transaction_id
    if present with the new transaction_id value.
    """
    mock_txn_id_fn.return_value.__enter__.return_value = "txn-local-20220802-000000001"
    mock_read_event_fn.side_effect = [
        create_event(evt, False) for evt in [ObsState.RESOURCING, ObsState.IDLE]
    ]

    mock_execute_fn.return_value = utils.MID_CN_ASSIGN_RESOURCES_SUCCESS_RESPONSE

    # creating command
    request = copy.deepcopy(mid_allocation_request_cdm)
    command = common.Command(
        utils.SKA_MID_CENTRAL_NODE_FQDN,
        "AssignResources",
        CODEC.dumps(request),
    )

    # simulating already present transaction_id
    request.transaction_id = "txn-local-20220802-251597784"

    _ = common._call_and_wait_for_transition(
        command, [ObsState.RESOURCING, ObsState.IDLE], "obsState"
    )

    # checking if the transaction_id got updated
    updated_transaction_id = re.search(r"\btxn-local.*", caplog.text).group(0)
    assert updated_transaction_id is not request.transaction_id

    mock_subscribe_event_fn.assert_called_once()
    mock_unsubscribe_event_fn.assert_called_once()
    mock_execute_fn.assert_called_once()


@mock.patch.object(common.EXECUTOR, "read_event")
@mock.patch.object(common.EXECUTOR, "execute")
def test_execute_command_and_wait_for_transition_raises_exception_when_error_state_encountered(
    mock_execute_fn, mock_read_event_fn
):
    """
    Verify that execute_command_and_wait_for_transition raises an exception when an error
    state is encountered.
    """
    # obsState will be SCANNING for the first three reads, then FAULT
    mock_read_event_fn.side_effect = [create_event(ObsState.FAULT, False)]

    # Test command to call SubArrayNode.Foo()
    cmd = common.Command(utils.SKA_MID_SUBARRAY_NODE_1_FQDN, "Foo")
    attribute = common.Attribute(cmd.device, "obsState")
    with pytest.raises(common.ValueTransitionError):
        common.execute_command_and_wait_for_transition(cmd, [ObsState.READY], attribute)
    mock_execute_fn.assert_called_once()


@pytest.mark.parametrize(
    ("input_request", "expected_request"),
    [
        ("mid_allocation_request_json", "mid_allocation_request_json"),
        ("mid_allocation_request_cdm", "mid_allocation_request_json"),
        ("mid_allocation_request_json_path", "mid_allocation_request_json"),
        ("low_allocation_request_json", "low_allocation_request_json"),
        ("low_allocation_request_cdm", "low_allocation_request_json"),
        ("low_allocation_request_json_path", "low_allocation_request_json"),
    ],
)
def test_get_request_json_assign_resources(input_request, expected_request, request):
    """
    Verify get_configure_json gets the same JSON string whether it is given a path to JSON file,
    a CDM object or a JSON string.
    """
    allocate_request = request.getfixturevalue(input_request)
    request_json = common.get_request_json(allocate_request, AssignResourcesRequest)
    expected_json = request.getfixturevalue(expected_request)

    assert request_json == expected_json


def test_get_request_json_assign_resources_no_processing(
    mid_allocation_request_json,
):
    """
    Verify get_configure_json does not validate JSON string when with_processing is set to False
    """
    allocation_request_json = json.loads(mid_allocation_request_json)
    del allocation_request_json["dish"]["receptor_ids"]  # Delete mandatory key
    allocate_request = json.dumps(allocation_request_json)

    # Missing mandatory key should throw an exception if input JSON is converted to CDM
    with pytest.raises(Exception):
        common.get_request_json(allocate_request, AssignResourcesRequest)

    # Function should not throw an error on missing key when processing is disabled
    common.get_request_json(
        allocate_request, AssignResourcesRequest, with_processing=False
    )


@pytest.mark.parametrize(
    "fixture_name",
    [
        "mid_allocation_invalid_json_semantic_validation",
        "low_allocation_invalid_json_semantic_validation",
    ],
)
def test_invalid_json_assign_resources_semantic_validation(fixture_name, request):
    """
    Verify semantic validation method for invalid json of assign resources
    """
    # TODO - remove this test
    # *WHY* is this test in ska-oso-scripting? I've boiled the implementation
    # down to the refactored version below, which tests whether CDM raises a
    # semantic validation error as the fixture loads the invalid file - but
    # surely that belongs to the ska-tmc-cdm project.
    with pytest.raises(SchematicValidationError):
        request.getfixturevalue(fixture_name)


@pytest.mark.parametrize(
    ("input_request", "expected_request"),
    [
        pytest.param(
            "mid_configure_request_json", "mid_configure_request_json", id="mid_json"
        ),
        pytest.param(
            "mid_configure_request_cdm", "mid_configure_request_json", id="mid_cdm"
        ),
        pytest.param(
            "mid_configure_request_json_path",
            "mid_configure_request_json",
            id="mid_path",
        ),
        pytest.param(
            "low_configure_request_json",
            "low_configure_request_json",
            id="low_json",
        ),
        pytest.param(
            "low_configure_request_cdm",
            "low_configure_request_json",
            id="low_cdm",
        ),
        pytest.param(
            "low_configure_request_json_path",
            "low_configure_request_json",
            id="low_path",
        ),
    ],
)
def test_get_request_json_configure(input_request, expected_request, request):
    """
    Verify get_configure_json gets the same JSON string whether it is given a path to JSON file,
    a CDM object or a JSON string.
    """
    config_request = request.getfixturevalue(input_request)
    request_json = common.get_request_json(config_request, ConfigureRequest)

    expected_json = request.getfixturevalue(expected_request)

    assert request_json == expected_json


@pytest.mark.parametrize(
    "json_path_fixture",
    [
        pytest.param(
            "path_to_semantically_invalid_configurerequest_json_for_low",
            id="invalid_low",
        ),
        pytest.param(
            "path_to_semantically_invalid_configurerequest_json_for_mid",
            id="invalid_mid",
        ),
    ],
)
def test_semantic_validation_exceptions_are_not_silenced_during_conversion(
    json_path_fixture, request
):
    """
    Verify that semantic validation errors are raised by
    common.get_request_json if syntactically correct but semantically invalid
    JSON is passed for conversion.
    """
    # should complain about bad JSON in a file...
    json_path = request.getfixturevalue(json_path_fixture)
    with pytest.raises(SchematicValidationError):
        _ = common.get_request_json(json_path, ConfigureRequest)

    # ... or in a string
    with open(json_path, "r", encoding="utf-8") as json_file:
        json_data = json_file.read()
    with pytest.raises(SchematicValidationError):
        _ = common.get_request_json(json_data, ConfigureRequest)

    # ... or in an object
    instance = CODEC.load_from_file(ConfigureRequest, json_path, validate=False)
    with pytest.raises(SchematicValidationError):
        _ = common.get_request_json(instance, ConfigureRequest)


def test_get_request_json_configure_no_processing(mid_configure_request_json):
    """
    Verify get_configure_json does not validate JSON string when with_processing is set to False
    """
    config_request_json = json.loads(mid_configure_request_json)
    del config_request_json["dish"]["receiver_band"]  # Delete mandatory key
    config_request = json.dumps(config_request_json)

    # Missing mandatory key should throw an exception if input JSON is converted to CDM
    with pytest.raises(Exception):
        _ = common.get_request_json(config_request, ConfigureRequest)

    try:
        _ = common.get_request_json(
            config_request, ConfigureRequest, with_processing=False
        )
    except Exception:
        # Function should not throw an error on missing key when processing is disabled
        assert False


@mock.patch.object(common.EXECUTOR, "execute")
@mock.patch.object(common.EXECUTOR, "subscribe_event")
@mock.patch.object(common.EXECUTOR, "unsubscribe_event")
@mock.patch.object(common, "transaction")
def test_call_and_wait_for_transition_and_raise_exception_assign_res_command_after_given_timeout(
    mock_txn_id_fn,
    mock_subscribe_event_fn,
    mock_unsubscribe_event_fn,
    mock_execute_fn,
):
    """
    Test that the call_and_wait_for_attribute_value function raises EventTimeoutError
    exception on given timeout value for assign resource command.
    """

    mock_txn_id_fn.return_value.__enter__.return_value = "txn-local-20220802-251597784"
    mock_execute_fn.return_value = utils.MID_CN_ASSIGN_RESOURCES_SUCCESS_RESPONSE

    # Test command to call SubArrayNode.Foo()
    cmd = common.Command(utils.SKA_MID_SUBARRAY_NODE_1_FQDN, "Foo")

    # This task waits for, in sequence, RESOURCING then IDLE.
    with pytest.raises(EventTimeoutError):
        _ = common._call_and_wait_for_transition(
            cmd,
            [ObsState.RESOURCING, ObsState.IDLE],
            "obsState",
            timeout=1,
        )

    mock_subscribe_event_fn.assert_called_once()
    mock_unsubscribe_event_fn.assert_called_once()
    # SubArrayNode.Foo() should just have been called once
    mock_execute_fn.assert_called_once()


@mock.patch.object(common.EXECUTOR, "execute")
@mock.patch.object(common.EXECUTOR, "subscribe_event")
@mock.patch.object(common.EXECUTOR, "unsubscribe_event")
@mock.patch.object(common, "transaction")
def test_call_and_wait_for_transition_and_raise_exception_configure_command_after_given_timeout(
    mock_txn_id_fn,
    mock_unsubscribe_event_fn,
    mock_subscribe_event_fn,
    mock_execute_fn,
    mid_configure_request_cdm,
    caplog,
):
    """
    Test that the call_and_wait_for_attribute_value function raises EventTimeoutError
    exception on given timeout value for configure command.
    """

    mock_txn_id_fn.return_value.__enter__.return_value = "txn-local-20220802-251597784"
    # creating command
    request = CODEC.dumps(mid_configure_request_cdm)
    command = common.Command(utils.SKA_MID_SUBARRAY_NODE_1_FQDN, "Configure", request)

    with pytest.raises(EventTimeoutError):
        _ = common._call_and_wait_for_transition(
            command,
            [ObsState.CONFIGURING, ObsState.READY],
            "obsState",
            timeout=1,
        )

    assert """ "transaction_id": "txn-local-""" in caplog.text

    mock_subscribe_event_fn.assert_called_once()
    mock_unsubscribe_event_fn.assert_called_once()
    mock_execute_fn.assert_called_once()


@mock.patch.object(common.EXECUTOR, "execute")
@mock.patch.object(common.EXECUTOR, "subscribe_event")
@mock.patch.object(common.EXECUTOR, "unsubscribe_event")
def test_call_and_wait_for_transition_and_raise_exception_scan_command_after_given_timeout(
    mock_unsubscribe_event_fn,
    mock_subscribe_event_fn,
    mock_execute_fn,
):
    """
    Test that the call_and_wait_for_attribute_value function raises EventTimeoutError
    exception on given timeout value for scan command.
    """

    # creating command
    command = common.Command(utils.SKA_MID_SUBARRAY_NODE_1_FQDN, "Scan")

    with pytest.raises(EventTimeoutError):
        _ = common._call_and_wait_for_transition(
            command,
            [ObsState.SCANNING, ObsState.READY],
            "obsState",
            timeout=1,
        )

    mock_subscribe_event_fn.assert_called_once()
    mock_unsubscribe_event_fn.assert_called_once()
    mock_execute_fn.assert_called_once()
