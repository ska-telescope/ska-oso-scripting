"""
Unit tests for the ska_oso_scripting.functions.devicecontrol module
"""
from pathlib import Path
from unittest import mock

import pytest
import tango
from ska_tango_base.control_model import ObsState
from tango import DevState

from ska_oso_scripting.functions import devicecontrol
from ska_oso_scripting.functions.devicecontrol import common
from ska_oso_scripting.functions.devicecontrol.exception import EventTimeoutError
from ska_oso_scripting.functions.devicecontrol.tango_executor import queue


def create_event(value, err: bool):
    """
    Create event with given value and error state
    """
    evt = mock.MagicMock(spec_set=tango.EventData)
    devattribute = mock.MagicMock(spec=tango.DeviceAttribute)
    devattribute.value = value
    evt.attr_value = devattribute
    evt.err = err
    return evt


@mock.patch.object(devicecontrol.resource_control, "assign_resources")
def test_assign_resources_from_file(mock_assign_fn):
    """
    Test that assign_resources_from_file calls the correct commands module function with correct parameters.
    """
    subarray_id = 1
    resource_path = "/mock/path/to/file.json"
    devicecontrol.assign_resources_from_file(subarray_id, resource_path, False)
    mock_assign_fn.assert_called_once_with(
        subarray_id, Path(resource_path), False, timeout=None
    )


@mock.patch.object(devicecontrol.resource_control, "assign_resources")
def test_assign_resources_from_cdm(mock_assign_fn, mid_allocation_request_cdm):
    """
    Test that assign_resources_from_cdm calls the correct commands module function with correct parameters.
    """
    subarray_id = 1
    devicecontrol.assign_resources_from_cdm(subarray_id, mid_allocation_request_cdm)
    mock_assign_fn.assert_called_once_with(
        subarray_id, mid_allocation_request_cdm, timeout=None
    )


@mock.patch.object(devicecontrol.resource_control, "assign_resources")
def test_assign_resources_from_json(mock_assign_fn, mid_allocation_request_json):
    """
    Test that assign_resources_from_cdm calls the correct commands module function with correct parameters.
    """
    subarray_id = 1
    devicecontrol.assign_resources_from_cdm(subarray_id, mid_allocation_request_json)
    mock_assign_fn.assert_called_once_with(
        subarray_id, mid_allocation_request_json, timeout=None
    )


@mock.patch.object(devicecontrol.resource_control, "release_resources")
def test_release_all_resources(mock_release_fn):
    """
    Test that release_all_resources calls the correct commands module function with correct parameters.
    """
    subarray_id = 1
    devicecontrol.release_all_resources(subarray_id)
    mock_release_fn.assert_called_once_with(subarray_id, release_all=True, timeout=None)


@mock.patch.object(devicecontrol.resource_control, "release_resources")
def test_release_resources(mock_release_fn):
    """
    Test that release_resources calls the correct commands module function with correct parameters.
    """
    subarray_id = 1
    mock_release_cdm = mock.MagicMock()
    devicecontrol.release_resources(subarray_id, mock_release_cdm)
    mock_release_fn.assert_called_once_with(
        subarray_id, release_all=False, request=mock_release_cdm, timeout=None
    )


@mock.patch.object(devicecontrol.subarray_control, "configure")
def test_configure_from_file(mock_configure_fn):
    """
    Test that configure_from_file calls the correct commands module function with correct parameters.
    """
    subarray_id = 1
    resource_path = "/mock/path/to/file.json"
    devicecontrol.configure_from_file(subarray_id, resource_path, False)
    mock_configure_fn.assert_called_once_with(
        subarray_id, Path(resource_path), False, timeout=None
    )


@mock.patch.object(devicecontrol.subarray_control, "configure")
@mock.patch(
    "ska_oso_scripting.functions.devicecontrol.WORKAROUNDS.extend_configure_timeout",
    new=False,
)
def test_configure_from_cdm(mock_configure_fn, mid_configure_request_cdm):
    """
    Test that configure_from_cdm calls the correct commands module function with correct parameters.
    """
    subarray_id = 1
    devicecontrol.configure_from_cdm(subarray_id, mid_configure_request_cdm)
    mock_configure_fn.assert_called_once_with(
        subarray_id, mid_configure_request_cdm, timeout=None
    )


@mock.patch.object(devicecontrol.subarray_control, "configure")
@mock.patch(
    "ska_oso_scripting.functions.devicecontrol.WORKAROUNDS.extend_configure_timeout",
    new=False,
)
def test_configure_from_json(mock_configure_fn, mid_configure_request_json):
    """
    Test that configure_from_cdm calls the correct commands module function with correct parameters.
    """
    subarray_id = 1

    devicecontrol.configure_from_cdm(subarray_id, mid_configure_request_json)
    mock_configure_fn.assert_called_once_with(
        subarray_id, mid_configure_request_json, timeout=None
    )


@mock.patch.object(devicecontrol.subarray_control, "scan")
def test_scan(mock_scan_fn):
    """
    Test that scab calls the correct commands module function with correct parameters.
    """
    subarray_id = 1
    devicecontrol.scan(subarray_id)
    mock_scan_fn.assert_called_once_with(subarray_id, timeout=None)


@mock.patch.object(devicecontrol.subarray_control, "end")
def test_end(mock_end_fn):
    """
    Test that end calls the correct commands module function with correct parameters.
    """
    subarray_id = 1
    devicecontrol.end(subarray_id)
    mock_end_fn.assert_called_once_with(subarray_id)


@mock.patch.object(devicecontrol.telescope_control, "telescope_on")
def test_telescope_on(mock_on_fn):
    """
    Test that telescope_on calls the correct commands module function with correct parameters.
    """
    devicecontrol.telescope_on()
    mock_on_fn.assert_called_once()


@mock.patch.object(devicecontrol.telescope_control, "telescope_off")
def test_telescope_off(mock_off_fn):
    """
    Test that telescope_off calls the correct commands module function with correct parameters.
    """
    final_state = DevState.OFF
    devicecontrol.telescope_off(final_state)
    mock_off_fn.assert_called_once_with(final_state)


@mock.patch.object(devicecontrol.subarray_recovery, "abort")
def test_abort(mock_abort_fn):
    """
    Test that abort calls the correct commands module function with correct parameters.
    """
    subarray_id = 1
    devicecontrol.abort(subarray_id)
    mock_abort_fn.assert_called_once_with(subarray_id)


@mock.patch.object(devicecontrol.subarray_recovery, "obsreset")
def test_obsreset(mock_obsreset_fn):
    """
    Test that obsreset calls the correct commands module function with correct parameters.
    """
    subarray_id = 1
    devicecontrol.obsreset(subarray_id)
    mock_obsreset_fn.assert_called_once_with(subarray_id)


@mock.patch.object(devicecontrol.subarray_recovery, "restart")
def test_restart(mock_restart_fn):
    """
    Test that restart calls the correct commands module function with correct parameters.
    """
    subarray_id = 1
    devicecontrol.restart(subarray_id)
    mock_restart_fn.assert_called_once_with(subarray_id)


@mock.patch.object(common.EXECUTOR, "execute")
@mock.patch.object(common.EXECUTOR, "subscribe_event")
@mock.patch.object(common.EXECUTOR, "unsubscribe_event")
def test_raise_exception_assign_resources_cmd_after_wait_on_given_timeout_value(
    mock_unsubscribe_event_fn,
    mock_subscribe_event_fn,
    mock_read_event_fn,
    mid_allocation_request_cdm,
):
    """
    Test that assign_resources_from_cdm calls the correct commands module function with correct parameters.
    and raise EventTimeoutError after waiting for read data from queue
    """
    subarray_id = 1
    with pytest.raises(EventTimeoutError):
        devicecontrol.assign_resources_from_cdm(
            subarray_id, mid_allocation_request_cdm, timeout=1
        )
    mock_subscribe_event_fn.assert_called_once()
    mock_unsubscribe_event_fn.assert_called_once()
    mock_read_event_fn.assert_called_once()


@mock.patch.object(common.EXECUTOR, "execute")
@mock.patch.object(common.EXECUTOR, "subscribe_event")
@mock.patch.object(common.EXECUTOR, "unsubscribe_event")
def test_raise_exception_configure_cmd_after_wait_on_given_timeout_value(
    mock_unsubscribe_event_fn,
    mock_subscribe_event_fn,
    mock_configure_event_fn,
    mid_configure_request_json,
):
    """
    Test that configure_from_cdm calls the correct commands module function with correct parameters.
    and raise EventTimeoutError after waiting for read data from queue
    """
    subarray_id = 1

    with pytest.raises(EventTimeoutError):
        devicecontrol.configure_from_cdm(
            subarray_id, mid_configure_request_json, timeout=1
        )
    mock_subscribe_event_fn.assert_called_once()
    mock_unsubscribe_event_fn.assert_called_once()
    mock_configure_event_fn.assert_called_once()


@mock.patch.object(common.EXECUTOR, "execute")
@mock.patch.object(common.EXECUTOR, "subscribe_event")
@mock.patch.object(common.EXECUTOR, "unsubscribe_event")
@mock.patch.object(devicecontrol.subarray_control, "_fetch_global_scan_id")
def test_raise_exception_scan_cmd_after_wait_on_given_timeout_value(
    mock_scan_id_fn,
    mock_unsubscribe_event_fn,
    mock_subscribe_event_fn,
    mock_scan_event_fn,
):
    """
    Test that scan call the correct commands module function with correct parameters.
    and raise EventTimeoutError after waiting for read data from queue
    """
    subarray_id = 1

    with pytest.raises(EventTimeoutError):
        devicecontrol.scan(subarray_id, timeout=0.01)
    mock_subscribe_event_fn.assert_called_once()
    mock_unsubscribe_event_fn.assert_called_once()
    mock_scan_event_fn.assert_called_once()
    mock_scan_id_fn.assert_called_once()


@mock.patch.object(common.EXECUTOR, "execute")
@mock.patch.object(common.EXECUTOR, "subscribe_event")
@mock.patch.object(common.EXECUTOR, "unsubscribe_event")
def test_raise_exception_release_cmd_after_wait_on_given_timeout_value(
    mock_unsubscribe_event_fn,
    mock_subscribe_event_fn,
    mock_release_event_fn,
):
    """
    Test that release_all_resources calls the correct commands module function with correct parameters.
    and raise EventTimeoutError after waiting for read data from queue
    """
    subarray_id = 1
    with pytest.raises(EventTimeoutError):
        devicecontrol.release_all_resources(subarray_id, timeout=1)
    mock_subscribe_event_fn.assert_called_once()
    mock_unsubscribe_event_fn.assert_called_once()
    mock_release_event_fn.assert_called_once()


@mock.patch.object(common.EXECUTOR, "execute")
@mock.patch.object(common.EXECUTOR, "subscribe_event")
@mock.patch.object(common.EXECUTOR, "unsubscribe_event")
@mock.patch.object(queue.Queue, "get")
@mock.patch.object(common, "transaction")
def test_assign_resources_from_cdm_after_adding_timeout(
    mock_txn_id_fn,
    mock_read_event,
    mock_unsubscribe_event_fn,
    mock_subscribe_event_fn,
    mock_read_event_fn,
    mid_allocation_request_cdm,
):
    """
    Test that assign_resources_from_cdm calls the correct commands module function with correct parameters.
    and complete before given timeout value
    """
    mock_txn_id_fn.return_value.__enter__.return_value = "txn-local-20220802-000000001"
    mock_read_event.side_effect = [
        create_event(evt, False) for evt in [ObsState.RESOURCING, ObsState.IDLE]
    ]
    subarray_id = 1
    devicecontrol.assign_resources_from_cdm(
        subarray_id, mid_allocation_request_cdm, timeout=3
    )
    mock_subscribe_event_fn.assert_called_once()
    mock_unsubscribe_event_fn.assert_called_once()
    mock_read_event_fn.assert_called_once()
