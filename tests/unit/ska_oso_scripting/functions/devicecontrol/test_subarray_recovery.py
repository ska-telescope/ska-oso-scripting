from unittest import mock

from ska_tango_base.control_model import ObsState

from ska_oso_scripting.functions.devicecontrol import subarray_recovery

from ... import utils


def test_get_obsreset_command():
    """
    Verify that a 'ObsReset' Command is targeted and structured correctly.
    """
    subarray_id = 1
    cmd = subarray_recovery.get_obsreset_command(subarray_id)
    assert cmd.device == utils.SKA_MID_SUBARRAY_NODE_1_FQDN
    assert cmd.command_name == "ObsReset"
    assert not cmd.args


def test_get_restart_command():
    """
    Verify that a 'restart' Command is targeted and structured correctly.
    """
    subarray_id = 1
    cmd = subarray_recovery.get_restart_command(subarray_id)
    assert cmd.device == utils.SKA_MID_SUBARRAY_NODE_1_FQDN
    assert cmd.command_name == "Restart"
    assert not cmd.args


@mock.patch.object(subarray_recovery, "_call_and_wait_for_transition")
def test_abort_defines_obsstate_transitions_correctly(mock_fn):
    """
    Verify that the abort observing task defines the correct command and
    happy path target obsStates.
    """
    subarray_id = 1
    subarray_recovery.abort(subarray_id)

    utils.validate_call_and_wait_for_transition_args(
        mock_fn,  # pass in mock function used for this test
        "Abort",  # 'abort' command is requested
        utils.SKA_MID_SUBARRAY_NODE_1_FQDN,  # command sent to SAN1, obsState read from SAN1
        "obsState",  # abort command should wait on the obsState
        [
            ObsState.ABORTING,
            ObsState.ABORTED,
        ],  # happy path sequence is ABORTED
    )


def test_get_abort_command():
    """
    Verify that a 'abort' Command is targeted and structured correctly.
    """
    subarray_id = 1
    cmd = subarray_recovery.get_abort_command(subarray_id)
    assert cmd.device == utils.SKA_MID_SUBARRAY_NODE_1_FQDN
    assert cmd.command_name == "Abort"
    assert not cmd.args


@mock.patch.object(subarray_recovery, "_call_and_wait_for_transition")
def test_obsreset_defines_obsstate_transitions_correctly(mock_fn):
    """
    Verify that the obsreset observing task defines the correct command and
    happy path target obsStates.
    """
    subarray_id = 1
    subarray_recovery.obsreset(subarray_id)

    utils.validate_call_and_wait_for_transition_args(
        mock_fn,  # pass in mock function used for this test
        "ObsReset",  # 'obsreset' command is requested
        utils.SKA_MID_SUBARRAY_NODE_1_FQDN,  # command sent to SAN1, obsState read from SAN1
        "obsState",  # reset command should wait on the obsState
        [
            ObsState.RESETTING,
            ObsState.IDLE,
        ],  # happy path sequence is first RESETTING then IDLE
    )


@mock.patch.object(subarray_recovery, "_call_and_wait_for_transition")
def test_restart_defines_obsstate_transitions_correctly(mock_fn):
    """
    Verify that the restart observing task defines the correct command and
    happy path target obsStates.
    """
    subarray_id = 1
    subarray_recovery.restart(subarray_id)

    utils.validate_call_and_wait_for_transition_args(
        mock_fn,  # pass in mock function used for this test
        "Restart",  # 'restart' command is requested
        utils.SKA_MID_SUBARRAY_NODE_1_FQDN,  # command sent to SAN1, obsState read from SAN1
        "obsState",  # restart command should wait on the obsState
        [
            ObsState.RESTARTING,
            ObsState.EMPTY,
        ],  # happy path sequence is RESTARTING then EMPTY
    )
