import os
import unittest.mock as mock

import pytest
from ska_ost_osd.telvalidation.schematic_validation_exceptions import (
    SchematicValidationError,
)
from ska_tango_base.control_model import ObsState
from ska_tmc_cdm.messages.subarray_node.configure import ConfigureRequest
from ska_tmc_cdm.schemas import CODEC

from ska_oso_scripting.functions.devicecontrol import subarray_control
from ska_oso_scripting.functions.devicecontrol.common import Command
from ska_oso_scripting.functions.devicecontrol.exception import SkuidRuntimeError

from ... import utils

VALID_LOW_STARTSCAN_REQUEST = """{
    "interface": "https://schema.skao.int/ska-low-tmc-scan/3.0",
    "scan_id": 123
}"""

VALID_ASSIGN_STARTSCAN_REQUEST = """{
    "interface": "https://schema.skao.int/ska-tmc-scan/2.1",
    "scan_id": 123
}"""


@mock.patch.object(subarray_control, "_call_and_wait_for_transition")
def test_configure_defines_obsstate_transitions_correctly(
    mock_fn, mid_configure_request_cdm
):
    """
    Verify that configure defines obsstate transitions correctly
    """
    subarray_id = 1
    subarray_control.configure(subarray_id, mid_configure_request_cdm)

    utils.validate_call_and_wait_for_transition_args(
        mock_fn,  # pass in mock function used for this test
        "Configure",  # 'scan' command is requested
        utils.SKA_MID_SUBARRAY_NODE_1_FQDN,  # command sent to SAN1, obsState read from SAN1
        "obsState",  # scan command should wait on the obsState
        [
            ObsState.CONFIGURING,
            ObsState.READY,
        ],
    )


def test_scan_forms_correct_command():
    """
    Tests that get_scan_command generates correct Command
    """
    subarray_id = 1

    with mock.patch(
        "ska_oso_scripting.functions.devicecontrol.subarray_control._fetch_global_scan_id",
    ) as mock_scan_id:
        mock_scan_id.return_value = 123
        generated = subarray_control.get_scan_command(subarray_id)

    expected = Command(
        utils.SKA_MID_SUBARRAY_NODE_1_FQDN,
        "Scan",
        VALID_ASSIGN_STARTSCAN_REQUEST,
    )
    utils.command_non_strict_equals(generated, expected)


@mock.patch.object(subarray_control.TANGO_REGISTRY, "get_subarray_node")
def test_scan_forms_correct_command_for_low(mock_get_subarray_node_fn):
    """
    Tests that get_scan_command generates correct Command for SKA-low
    """
    mock_get_subarray_node_fn.return_value = utils.SKA_LOW_SUBARRAY_NODE_1_FQDN
    subarray_id = 1

    with mock.patch.dict(os.environ, {"SKA_TELESCOPE": "low"}):
        with mock.patch(
            "ska_oso_scripting.functions.devicecontrol.subarray_control._fetch_global_scan_id",
        ) as mock_scan_id:
            mock_scan_id.return_value = 123
            generated = subarray_control.get_scan_command(subarray_id)

    expected = Command(
        utils.SKA_LOW_SUBARRAY_NODE_1_FQDN, "Scan", VALID_LOW_STARTSCAN_REQUEST
    )
    utils.command_non_strict_equals(generated, expected)


@mock.patch.object(subarray_control, "_call_and_wait_for_transition")
@mock.patch.object(subarray_control, "_fetch_global_scan_id")
def test_scan_defines_obsstate_transitions_correctly(_, mock_fn):
    """
    Verify that scan defines the obsstate transitions correctly.
    """
    subarray_id = 1
    subarray_control.scan(subarray_id)

    utils.validate_call_and_wait_for_transition_args(
        mock_fn,  # pass in mock function used for this test
        "Scan",  # 'scan' command is requested
        utils.SKA_MID_SUBARRAY_NODE_1_FQDN,  # command sent to SAN1, obsState read from SAN1
        "obsState",  # scan command should wait on the obsState
        [
            ObsState.SCANNING,
            ObsState.READY,
        ],  # happy path sequence is SCANNING, READY
    )


@mock.patch.object(subarray_control, "_call_and_wait_for_transition")
def test_scan_raises_skuid_runtime_error(mock_fn):
    """
    Verify that scan raises SkuidRuntimeError when SKUID is not available or the
    correct environment variable is not set.
    """
    if "SKUID_URL" in os.environ:
        current_skuid_url = os.environ["SKUID_URL"]
        del os.environ["SKUID_URL"]
    else:
        current_skuid_url = None

    subarray_id = 1

    with pytest.raises(SkuidRuntimeError) as e:
        subarray_control.scan(subarray_id)
        assert "SKUID_URL environment variable not set" in e.traceback

    invalid_skuid_url = "http://foo.bar"
    os.environ["SKUID_URL"] = invalid_skuid_url

    with pytest.raises(SkuidRuntimeError) as e:
        subarray_control.scan(subarray_id)
        assert f"Could not fetch scan ID from {invalid_skuid_url}" in e.traceback

    if current_skuid_url:
        # Restore SKUID_URL for test run if it was set before
        os.environ["SKUID_URL"] = current_skuid_url


def test_get_end_command():
    """
    Verify that an 'end' Command is targeted and structured correctly.
    """
    subarray_id = 1
    cmd = subarray_control.get_end_command(subarray_id)
    assert cmd.device == utils.SKA_MID_SUBARRAY_NODE_1_FQDN
    assert cmd.command_name == "End"
    assert not cmd.args


@mock.patch.object(subarray_control, "_call_and_wait_for_transition")
def test_end_defines_obsstate_transitions_correctly(mock_fn):
    """
    Verify that the end observing task defines the correct command and
    happy path target obsStates.
    """
    subarray_id = 1
    subarray_control.end(subarray_id)

    utils.validate_call_and_wait_for_transition_args(
        mock_fn,  # pass in mock function used for this test
        "End",  # 'end' command is requested
        utils.SKA_MID_SUBARRAY_NODE_1_FQDN,  # command sent to SAN1, obsState read from SAN1
        "obsState",  # end command should wait on the obsState
        [ObsState.IDLE],  # happy path sequence is IDLE
    )


@pytest.mark.parametrize(
    "json_path_fixture",
    [
        pytest.param(
            "path_to_semantically_invalid_configurerequest_json_for_low",
            id="invalid_file_low",
        ),
        pytest.param(
            "path_to_semantically_invalid_configurerequest_json_for_mid",
            id="invalid_file_mid",
        ),
    ],
)
def test_configure_raises_semantic_validation_errors(json_path_fixture, request):
    """
    Verify that semantic validation errors are not masked by configure().
    """
    subarray_id = 1

    # should complain about bad JSON in a file...
    json_path = request.getfixturevalue(json_path_fixture)
    with pytest.raises(SchematicValidationError):
        subarray_control.configure(subarray_id, json_path)

    # ... or in a string
    with open(json_path, "r", encoding="utf-8") as json_file:
        json_str = json_file.read()
    with pytest.raises(SchematicValidationError):
        subarray_control.configure(subarray_id, json_str)

    # ... or in an object
    instance = CODEC.load_from_file(ConfigureRequest, json_path, validate=False)
    with pytest.raises(SchematicValidationError):
        subarray_control.configure(subarray_id, instance)
