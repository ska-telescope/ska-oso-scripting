"""
Unit tests for the ska_oso_scripting.sb module
"""
import os
from pathlib import Path
from unittest import mock

import pytest
from ska_oso_pdm.sb_definition import SBDefinition
from ska_ser_skuid.client import SkuidClient
from ska_ser_skuid.services import SKAEntityTypeService

from ska_oso_scripting.functions import sb

TEST_RESOURCES = Path(__file__).parents[3] / "resources"


@pytest.fixture
def mid_sb_json_path():
    """
    Pytest fixture to return configure JSON for SKA-mid
    """
    json_path = (
        TEST_RESOURCES / "integration_test_cases/testfile_nonsidereal_sb_mid.json"
    )
    return json_path


@pytest.fixture
def low_sb_json_path():
    """
    Pytest fixture to return configure JSON for SKA-low
    """
    json_path = (
        TEST_RESOURCES
        / "pdm_transforms_test_cases/low_no_tied_array_no_sdp/sb_definition.json"
    )
    return json_path


@mock.patch.dict(os.environ, {"SKUID_URL": "http://localhost:4000/no/where"})
@mock.patch.object(SkuidClient, "fetch_skuid")
def test_create_mid_sbi(mock_fetch_fn, mid_sb_json_path):
    """
    Verify that create_sbi updates the SB ID
    """
    # set up mock. SB IDs will be sbi1, sbi2, sbi3, etc., while
    # processing block IDs will be pb1, pb2, pb3, etc.
    counter = {}

    def side_effect(entity_type: SKAEntityTypeService.DefaultEntityTypes):
        entity_counter = counter.setdefault(entity_type, 1)
        counter[entity_type] += 1
        return f"{entity_type.value}{entity_counter}"

    mock_fetch_fn.side_effect = side_effect

    sbd = sb.load_sbd(mid_sb_json_path)
    sbi = sb.create_sbi(sbd)

    # Check the sb id has changed
    assert sbi.sbd_id.startswith("sbi")
    assert sbi.sbd_id != sbd.sbd_id

    # Check for the eb_ids
    sbi_sdp_config = sbi.sdp_configuration
    sbd_sdp_config = sbd.sdp_configuration

    assert sbi_sdp_config.execution_block.eb_id.startswith("eb")
    assert sbi_sdp_config.execution_block.eb_id != sbd_sdp_config.execution_block.eb_id

    # Loop through the processing blocks using zip
    sbi_pbs = sbi_sdp_config.processing_blocks
    sbd_pbs = sbd_sdp_config.processing_blocks
    for sbi_pb, sbd_pb in zip(sbi_pbs, sbd_pbs):
        assert sbi_pb.pb_id.startswith("pb")
        assert sbi_pb.pb_id != sbd_pb.pb_id
        assert sbi.sbd_id in sbi_pb.sbi_refs


@mock.patch.dict(os.environ, {"SKUID_URL": "http://localhost:4000/no/where"})
@mock.patch.object(SkuidClient, "fetch_skuid")
def test_create_low_sbi(mock_fetch_fn, low_sb_json_path):
    """
    Verify that create_sbi updates the SB ID
    """
    # set up mock. SB IDs will be sbi1, sbi2, sbi3, etc., while@mock.patch("src.ska_oso_scripting.functions.sb.SBDefinition.model_validate_json")
    # processing block IDs will be pb1, pb2, pb3, etc.
    counter = {}

    def side_effect(entity_type: SKAEntityTypeService.DefaultEntityTypes):
        entity_counter = counter.setdefault(entity_type, 1)
        counter[entity_type] += 1
        return f"{entity_type.value}{entity_counter}"

    mock_fetch_fn.side_effect = side_effect

    sbd = sb.load_sbd(low_sb_json_path)
    sbi = sb.create_sbi(sbd)

    # Check the sb id has changed
    assert sbi.sbd_id != sbd.sbd_id


def test_load_sbd_failure():
    with pytest.raises(IOError):
        sb.load_sbd("invalid_file.json")


def test_load_sbd_success(mid_sb_json_path):
    with open(mid_sb_json_path) as fh:
        data = str(fh.read())
        expected = SBDefinition.model_validate_json(data)
    with mock.patch("builtins.open", mock.mock_open(read_data=data)) as mock_file:
        result = sb.load_sbd(mid_sb_json_path)
        mock_file.assert_called_once_with(mid_sb_json_path, "r", encoding="utf-8")
        assert result == expected
