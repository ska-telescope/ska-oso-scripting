"""
Unit tests for the ska_oso_scripting.pdm_transforms.sdp_convert module.

The purpose of these tests is to confirm that the pdm_transforms code converts
CDM entities to the expected equivalent PDM entities.
"""
import copy
import unittest
from datetime import timedelta

import pytest
from freezegun import freeze_time
from ska_oso_pdm.sb_definition import ScanDefinition
from ska_oso_pdm.sb_definition.sdp import (
    BeamMapping,
    Channels,
    ExecutionBlock,
    PbDependency,
    Polarisation,
    ProcessingBlock,
    Resources,
    ScanType,
    Script,
    ScriptKind,
    SDPConfiguration,
    SpectralWindow,
)
from ska_tmc_cdm.messages.central_node.sdp import Channel as cdm_Channel
from ska_tmc_cdm.messages.central_node.sdp import (
    ChannelConfiguration as cdm_ChannelConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import EBScanType as cdm_EBScanType
from ska_tmc_cdm.messages.central_node.sdp import EBScanTypeBeam as cdm_ScanTypeBeam
from ska_tmc_cdm.messages.central_node.sdp import (
    ExecutionBlockConfiguration as cdm_EBConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import PbDependency as cdm_PbDependency
from ska_tmc_cdm.messages.central_node.sdp import (
    PolarisationConfiguration as cdm_PolarisationConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import (
    ProcessingBlockConfiguration as cdm_ProcessingBlockConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import (
    ScriptConfiguration as cdm_ScriptConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import (
    SDPConfiguration as cdm_centralnode_SDPConfiguration,
)
from ska_tmc_cdm.messages.subarray_node.configure.sdp import (
    SDPConfiguration as cdm_subarraynode_SDPConfiguration,
)

from ska_oso_scripting.functions.pdm_transforms.sdp_convert import (
    convert_beam,
    convert_beam_mapping,
    convert_channels,
    convert_dependencies,
    convert_execution_block,
    convert_polarisation,
    convert_processing_block,
    convert_resources,
    convert_scantypes,
    convert_script,
    convert_sdpconfiguration_centralnode,
    convert_sdpconfiguration_subarraynode,
    convert_spectral_window,
)
from tests.unit.ska_oso_scripting.builders.cdm.sdp.beamconfiguration import (
    BeamConfigurationBuilder,
)
from tests.unit.ska_oso_scripting.builders.pdm.sdp.beam import BeamBuilder
from tests.unit.ska_oso_scripting.functions.pdm_transforms.test_sdp_create import (
    TestTargetConversion,
)

SCRIPT1 = Script(name="ical", kind=ScriptKind.BATCH, version="0.1.0")
SCRIPTCONFIGURATION1 = cdm_ScriptConfiguration(
    name="ical", kind="batch", version="0.1.0"
)

SCRIPT2 = Script(name="vis_receive", kind=ScriptKind.REALTIME, version="2.1.1")
SCRIPTCONFIGURATION2 = cdm_ScriptConfiguration(
    name="vis_receive", kind="realtime", version="2.1.1"
)


class SDPConfigurationConversionSubarrayNodeTests(unittest.TestCase):
    """
    Unit tests for converting SDPConfiguration PDM to CDM transforms for
    subarray node.
    """

    @staticmethod
    def test_convert_subarraynode_sdpconfiguration_converts_to_expected_cdm_entity():
        sample_duration = 12
        scan_type = "science_A"
        sample_scan_def = ScanDefinition(
            scan_definition_id="foo",
            scan_duration_ms=timedelta(seconds=sample_duration),
            target_ref="typical",
            dish_configuration_ref="dish config 123",
            scan_type_ref=scan_type,
        )
        expected = cdm_subarraynode_SDPConfiguration(scan_type=scan_type)
        actual = convert_sdpconfiguration_subarraynode(sample_scan_def)
        assert actual == expected

    @staticmethod
    def test_convert_subarraynode_sdpconfiguration_complains_on_invalid_type():
        """
        Verify that convert_tmcconfiguration checks input argument type.
        """
        with pytest.raises(TypeError):
            convert_sdpconfiguration_subarraynode(1)


class TestMidSDPTransformConversions:
    """
    Unit tests for getting target ra, dec and system from PDM Target
    """

    SPECTRAL_WINDOW_1 = SpectralWindow(
        spectral_window_id="foo",
        count=744,
        start=2000,
        stride=1,
        freq_min=0.36e9,
        freq_max=0.368e9,
        link_map=[(2000, 4), (2200, 5)],
    )
    EXPECTED_CHANNEL1 = cdm_Channel(
        count=744,
        start=2000,
        stride=1,
        freq_min=0.36e9,
        freq_max=0.368e9,
        link_map=[(2000, 4), (2200, 5)],
        spectral_window_id="foo",
    )

    SPECTRAL_WINDOW_2 = SpectralWindow(
        spectral_window_id="bar",
        count=744,
        start=0,
        stride=2,
        freq_min=0.35e9,
        freq_max=0.368e9,
        link_map=[(0, 0), (200, 1), (744, 2), (944, 3)],
    )
    EXPECTED_CHANNEL2 = cdm_Channel(
        count=744,
        start=0,
        stride=2,
        freq_min=0.35e9,
        freq_max=0.368e9,
        link_map=[(0, 0), (200, 1), (744, 2), (944, 3)],
        spectral_window_id="bar",
    )

    CHANNELS1 = Channels(
        channels_id="channel_id_1", spectral_windows=[SPECTRAL_WINDOW_1]
    )
    CHANNELS2 = Channels(
        channels_id="channel_id_2", spectral_windows=[SPECTRAL_WINDOW_2]
    )

    EXPECTED_CHANNEL_CONFIGURATION1 = cdm_ChannelConfiguration(
        channels_id="channel_id_1",
        spectral_windows=[EXPECTED_CHANNEL1],
    )

    EXPECTED_CHANNEL_CONFIGURATION2 = cdm_ChannelConfiguration(
        channels_id="channel_id_2",
        spectral_windows=[EXPECTED_CHANNEL2],
    )

    BEAM_MAPPING1 = BeamMapping(beam_ref="vis0", field_ref="field_a")
    EXPECTED_EB_SCANTYPE_BEAM1 = cdm_ScanTypeBeam(field_id="field_a")
    BEAM_MAPPING2 = BeamMapping(
        beam_ref="pss1", channels_ref="pulsar_channels", polarisations_ref="all"
    )
    EXPECTED_EB_SCANTYPE_BEAM2 = cdm_ScanTypeBeam(
        channels_id="pulsar_channels", polarisations_id="all"
    )

    SCANTYPE1 = ScanType(
        scan_type_id="science_A",
        derive_from=".default",
        beams=[BEAM_MAPPING1, BEAM_MAPPING2],
    )
    EXPECTED_EB_SCANTYPE1 = cdm_EBScanType(
        scan_type_id="science_A",
        beams={
            "vis0": EXPECTED_EB_SCANTYPE_BEAM1,
            "pss1": EXPECTED_EB_SCANTYPE_BEAM2,
        },
        derive_from=".default",
    )

    SCANTYPE2 = ScanType(scan_type_id="science_B", beams=[BEAM_MAPPING1])
    EXPECTED_EB_SCANTYPE2 = cdm_EBScanType(
        scan_type_id="science_B", beams={"vis0": EXPECTED_EB_SCANTYPE_BEAM1}
    )

    POLARISATION1 = Polarisation(
        polarisations_id="pol_a", corr_type=["XX", "XY", "YY", "YX"]
    )
    EXPECTED_POLARISATION1 = cdm_PolarisationConfiguration(
        polarisations_id="pol_a", corr_type=["XX", "XY", "YY", "YX"]
    )

    POLARISATION2 = Polarisation(polarisations_id="pol_b", corr_type=["XX", "YX"])
    EXPECTED_POLARISATION2 = cdm_PolarisationConfiguration(
        polarisations_id="pol_b", corr_type=["XX", "YX"]
    )

    RESOURCES1 = Resources(
        csp_links=[1, 2, 3, 4], receptors=["FS4", "FS8"], receive_nodes=10
    )
    EXPECTED_RESOURCES1 = {
        "csp_links": [1, 2, 3, 4],
        "receptors": ["FS4", "FS8"],
        "receive_nodes": 10,
    }
    RESOURCES2 = Resources(
        csp_links=[1, 2],
    )
    EXPECTED_RESOURCES2 = {"csp_links": [1, 2]}

    DEPENDENCY1 = PbDependency(pb_ref="pb-mvp01-20200325-00001", kind=["visibilities"])
    EXPECTED_DEPENDENCY1 = cdm_PbDependency(
        pb_id="pb-mvp01-20200325-00001", kind=["visibilities"]
    )

    DEPENDENCY2 = PbDependency(pb_ref="pb-mvp01-20200325-00003", kind=["calibration"])
    EXPECTED_DEPENDENCY2 = cdm_PbDependency(
        pb_id="pb-mvp01-20200325-00003", kind=["calibration"]
    )

    PROCESSINGBLOCK1 = ProcessingBlock(
        pb_id="pb-mvp01-20220711-00001",
        script=SCRIPT1,
        parameters={},
        dependencies=[DEPENDENCY1],
        sbi_refs=["sbi-mvp01-20300425-00001"],
    )
    EXPECTED_PROCESSINGBLOCK1 = cdm_ProcessingBlockConfiguration(
        pb_id="pb-mvp01-20220711-00001",
        script=SCRIPTCONFIGURATION1,
        parameters={},
        dependencies=[EXPECTED_DEPENDENCY1],
        sbi_ids=["sbi-mvp01-20300425-00001"],
    )

    PROCESSINGBLOCK2 = ProcessingBlock(
        pb_id="pb-mvp01-20220711-00002",
        script=SCRIPT2,
        parameters={},
        dependencies=[DEPENDENCY2],
        sbi_refs=["sbi-mvp01-20300425-00002"],
    )
    EXPECTED_PROCESSINGBLOCK2 = cdm_ProcessingBlockConfiguration(
        pb_id="pb-mvp01-20220711-00002",
        script=SCRIPTCONFIGURATION2,
        parameters={},
        dependencies=[EXPECTED_DEPENDENCY2],
        sbi_ids=["sbi-mvp01-20300425-00002"],
    )

    def test_convert_spectral_window(self):
        actual1 = convert_spectral_window(self.SPECTRAL_WINDOW_1)
        actual2 = convert_spectral_window(self.SPECTRAL_WINDOW_2)
        assert actual1 == self.EXPECTED_CHANNEL1
        assert actual2 == self.EXPECTED_CHANNEL2
        assert actual1 != self.EXPECTED_CHANNEL2

    def test_convert_resources(self):
        actual1 = convert_resources(self.RESOURCES1)
        actual2 = convert_resources(self.RESOURCES2)
        assert actual1 == self.EXPECTED_RESOURCES1
        assert actual2 == self.EXPECTED_RESOURCES2
        assert actual1 != self.EXPECTED_RESOURCES2

    def test_convert_channels(self):
        actual1 = convert_channels(self.CHANNELS1)
        actual2 = convert_channels(self.CHANNELS2)
        assert actual1 == self.EXPECTED_CHANNEL_CONFIGURATION1
        assert actual2 == self.EXPECTED_CHANNEL_CONFIGURATION2
        assert actual1 != self.EXPECTED_CHANNEL2

    def test_convert_scantypes(self):
        actual1 = convert_scantypes(self.SCANTYPE1)
        actual2 = convert_scantypes(self.SCANTYPE2)
        assert actual1 == self.EXPECTED_EB_SCANTYPE1
        assert actual2 == self.EXPECTED_EB_SCANTYPE2
        assert actual1 != self.EXPECTED_EB_SCANTYPE2

    def test_convert_beam_mapping(self):
        actual1 = convert_beam_mapping(self.BEAM_MAPPING1)
        actual2 = convert_beam_mapping(self.BEAM_MAPPING2)
        assert actual1 == self.EXPECTED_EB_SCANTYPE_BEAM1
        assert actual2 == self.EXPECTED_EB_SCANTYPE_BEAM2
        assert actual1 != self.EXPECTED_EB_SCANTYPE_BEAM2

    def test_convert_polarisation(self):
        actual1 = convert_polarisation(self.POLARISATION1)
        actual2 = convert_polarisation(self.POLARISATION2)
        assert actual1 == self.EXPECTED_POLARISATION1
        assert actual2 == self.EXPECTED_POLARISATION2
        assert actual1 != self.EXPECTED_POLARISATION2

    def test_convert_dependencies(self):
        actual1 = convert_dependencies(self.DEPENDENCY1)
        actual2 = convert_dependencies(self.DEPENDENCY2)
        assert actual1 == self.EXPECTED_DEPENDENCY1
        assert actual2 == self.EXPECTED_DEPENDENCY2
        assert actual1 != self.EXPECTED_DEPENDENCY2

    @pytest.mark.parametrize(
        "pdm_instance,expected",
        [(SCRIPT1, SCRIPTCONFIGURATION1), (SCRIPT2, SCRIPTCONFIGURATION2)],
    )
    def test_convert_script(self, pdm_instance, expected):
        """
        Verify that PDM Script is converted to a CDM ScriptConfiguration correctly.
        """
        actual = convert_script(pdm_instance)
        assert actual == expected

    def test_convert_processing_block(self):
        actual1 = convert_processing_block(self.PROCESSINGBLOCK1)
        actual2 = convert_processing_block(self.PROCESSINGBLOCK2)
        assert actual1 == self.EXPECTED_PROCESSINGBLOCK1
        assert actual2 == self.EXPECTED_PROCESSINGBLOCK2
        assert actual1 != self.EXPECTED_PROCESSINGBLOCK2


class TestBeamConversion:
    """
    Tests to confirm that PDM Beams are converted to CDM BeamConfigurations correctly.
    """

    @pytest.mark.parametrize(
        "pdm_instance,expected",
        [
            (
                BeamBuilder().for_visibilities().with_beam_id("foo").build(),
                BeamConfigurationBuilder()
                .for_visibilities()
                .with_beam_id("foo")
                .build(),
            ),
            (
                BeamBuilder()
                .for_pss()
                .with_beam_id("foo")
                .with_search_beam_id(123)
                .build(),
                BeamConfigurationBuilder()
                .for_pss()
                .with_beam_id("foo")
                .with_search_beam_id(123)
                .build(),
            ),
            (
                BeamBuilder()
                .for_pst()
                .with_beam_id("foo")
                .with_timing_beam_id(123)
                .build(),
                BeamConfigurationBuilder()
                .for_pst()
                .with_beam_id("foo")
                .with_timing_beam_id(123)
                .build(),
            ),
            (
                BeamBuilder()
                .for_vlbi()
                .with_beam_id("foo")
                .with_vlbi_beam_id(123)
                .build(),
                BeamConfigurationBuilder()
                .for_vlbi()
                .with_beam_id("foo")
                .with_vlbi_beam_id(123)
                .build(),
            ),
            (
                BeamBuilder().for_transient_buffer().with_beam_id("foo").build(),
                BeamConfigurationBuilder()
                .for_transient_buffer()
                .with_beam_id("foo")
                .build(),
            ),
        ],
    )
    def test_conversion(self, pdm_instance, expected):
        """
        Verify that PDM Beam is converted to a CDM BeamConfiguration correctly.
        """
        converted = convert_beam(pdm_instance)
        assert converted == expected

    def test_raises_exception_when_given_object_of_wrong_type(self):
        with pytest.raises(TypeError):
            _ = convert_beam("foo")


class TestExecutionBlockConversion:
    """
    Tests to confirm that PDM ExecutionBlock object is converted to CDM ExecutionBlockConfiguration correctly.
    """

    EB = ExecutionBlock(
        eb_id="eb-123",
        max_length=10.0,
        context={},
        beams=[BeamBuilder().for_visibilities().with_beam_id("foo").build()],
        scan_types=[
            TestMidSDPTransformConversions.SCANTYPE1,
            TestMidSDPTransformConversions.SCANTYPE2,
        ],
        channels=[
            TestMidSDPTransformConversions.CHANNELS1,
            TestMidSDPTransformConversions.CHANNELS2,
        ],
        polarisations=[
            TestMidSDPTransformConversions.POLARISATION1,
            TestMidSDPTransformConversions.POLARISATION2,
        ],
    )

    EXPECTED_EB = cdm_EBConfiguration(
        eb_id="eb-123",
        max_length=10.0,
        context={},
        beams=[
            BeamConfigurationBuilder().for_visibilities().with_beam_id("foo").build()
        ],
        scan_types=[
            TestMidSDPTransformConversions.EXPECTED_EB_SCANTYPE1,
            TestMidSDPTransformConversions.EXPECTED_EB_SCANTYPE2,
        ],
        channels=[
            TestMidSDPTransformConversions.EXPECTED_CHANNEL_CONFIGURATION1,
            TestMidSDPTransformConversions.EXPECTED_CHANNEL_CONFIGURATION2,
        ],
        polarisations=[
            TestMidSDPTransformConversions.EXPECTED_POLARISATION1,
            TestMidSDPTransformConversions.EXPECTED_POLARISATION2,
        ],
        fields=[
            TestTargetConversion.CONVERTED_SINGLE_POINTING_NO_OFFSET,
            TestTargetConversion.CONVERTED_SINGLE_POINTING_WITH_OFFSET,
        ],
    )

    TARGETS = [
        TestTargetConversion.SINGLE_POINTING_NO_OFFSET,
        TestTargetConversion.SINGLE_POINTING_WITH_OFFSET,
    ]

    @freeze_time("2023-02-16T01:23:45Z")
    def test_convert_execution_block(self):
        actual1 = convert_execution_block(self.EB, self.TARGETS)
        assert actual1 == self.EXPECTED_EB
        wrong_id_eb = copy.deepcopy(self.EXPECTED_EB)
        wrong_id_eb.eb_id = "eb-234"
        assert wrong_id_eb != self.EXPECTED_EB


class SDPConfigurationConversionCentralNodeTests:
    """
    Tests to confirm that PDM SDPConfiguration object is converted to CDM
    SDPConfiguration for central node correctly.
    """

    SDP = SDPConfiguration(
        execution_block=TestExecutionBlockConversion.EB,
        resources=TestMidSDPTransformConversions.RESOURCES1,
        processing_blocks=[
            TestMidSDPTransformConversions.PROCESSINGBLOCK1,
            TestMidSDPTransformConversions.PROCESSINGBLOCK2,
        ],
    )

    EXPECTED_SDP = cdm_centralnode_SDPConfiguration(
        execution_block=TestExecutionBlockConversion.EXPECTED_EB,
        resources=TestMidSDPTransformConversions.EXPECTED_RESOURCES1,
        processing_blocks=[
            TestMidSDPTransformConversions.EXPECTED_PROCESSINGBLOCK1,
            TestMidSDPTransformConversions.EXPECTED_PROCESSINGBLOCK2,
        ],
    )

    @freeze_time("2023-02-16T01:23:45Z")
    def test_convert_sdpconfiguration(self):
        actual1 = convert_sdpconfiguration_centralnode(
            self.SDP, TestExecutionBlockConversion.TARGETS
        )
        assert actual1 == self.EXPECTED_SDP

    @pytest.mark.parametrize(
        "missing_param,empty_pmd_val, empty_cdm_val",
        [
            ("execution_block", None, None),
            ("resources", Resources(), {}),
            ("processing_blocks", [], []),
        ],
    )
    @freeze_time("2023-02-16T01:23:45Z")
    def test_convert_sdp_with_missing_optional_params(
        self, missing_param, empty_pmd_val, empty_cdm_val
    ):
        pdm_sdp = copy.deepcopy(self.SDP)
        cdm_sdp = copy.deepcopy(self.EXPECTED_SDP)
        setattr(pdm_sdp, missing_param, empty_pmd_val)
        setattr(cdm_sdp, missing_param, empty_cdm_val)
        actual = convert_sdpconfiguration_centralnode(
            pdm_sdp, TestExecutionBlockConversion.TARGETS
        )
        assert actual == cdm_sdp
