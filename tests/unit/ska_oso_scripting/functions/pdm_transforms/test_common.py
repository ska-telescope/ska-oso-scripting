"""
Unit tests for the ska_oso_scripting.pdm_transforms module.

The purpose of these tests is to confirm that the pdm_transforms code converts
CDM entities to the expected equivalent PDM entities.
"""
import unittest
from datetime import timedelta

import pytest
from astropy.units import Quantity
from ska_oso_pdm import Target
from ska_oso_pdm.sb_definition import (
    Beam,
    EquatorialCoordinates,
    EquatorialCoordinatesPST,
    EquatorialCoordinatesReferenceFrame,
    HorizontalCoordinates,
    RadialVelocity,
    RadialVelocityUnits,
    ScanDefinition,
    SolarSystemObject,
    SolarSystemObjectName,
)
from ska_tmc_cdm.messages.skydirection import AltAzField, ICRSField
from ska_tmc_cdm.messages.skydirection import SolarSystemObject as cdm_SSO
from ska_tmc_cdm.messages.skydirection import SpecialField
from ska_tmc_cdm.messages.subarray_node.configure.tmc import (
    TMCConfiguration as cdm_TMCConfiguration,
)

from ska_oso_scripting.functions.pdm_transforms import convert_tmcconfiguration
from ska_oso_scripting.functions.pdm_transforms.common import convert_target


class TMCConfigurationConversionTests(unittest.TestCase):
    """
    Unit tests for TMCConfiguration PDM to CDM transforms.
    """

    @staticmethod
    def test_convert_to_expected_cdm_entity_for_mid():
        sample_duration = 12
        sample_scan_def = ScanDefinition(
            scan_definition_id="foo",
            scan_duration_ms=timedelta(seconds=sample_duration),
            target_id="typical",
            dish_configuration_id="dish config 123",
        )
        expected = cdm_TMCConfiguration(
            scan_duration=timedelta(seconds=sample_duration)
        )
        actual = convert_tmcconfiguration(sample_scan_def)
        assert actual == expected

    @staticmethod
    def test_convert_to_expected_cdm_entity_for_low():
        sample_duration = 12
        sample_scan_def = ScanDefinition(
            scan_definition_id="bar",
            scan_duration_ms=timedelta(seconds=sample_duration),
            target_id="typical",
            target_beam_configuration_ids=[],
        )
        expected = cdm_TMCConfiguration(
            scan_duration=timedelta(seconds=sample_duration)
        )
        actual = convert_tmcconfiguration(sample_scan_def)
        assert actual == expected

    @staticmethod
    def test_convert_tmcconfiguration_complains_on_invalid_type():
        """
        Verify that convert_tmcconfiguration checks input argument type.
        """
        with pytest.raises(TypeError):
            convert_tmcconfiguration(1)


class TestTargetConversion:
    """
    Unit tests for the conversion of PDM coordinate classes to the
    equivalent CDM sky directions.
    """

    def test_icrs_beam_conversion(self):
        """
        Test that a PDM beam is converted to the expected CDM ICRSField.
        """
        beam = Beam(
            beam_id=1,
            beam_coordinate=EquatorialCoordinatesPST(
                target_id="foo",
                reference_frame=EquatorialCoordinatesReferenceFrame.ICRS,
                ra_str="01:20:00.00",
                dec_str="-45:30:00",
                pm_ra=1.0,
                pm_dec=2.0,
                epoch=2010.5,
                parallax=3.0,
            ),
        )
        expected = ICRSField(
            target_name="foo",
            attrs=ICRSField.Attrs(
                # astropy doesn't convert cleanly
                # c1=20.0,
                c1=19.999999999999996,
                c2=-45.5,
                pm_c1=1.0,
                pm_c2=2.0,
                parallax=3.0,
                epoch=2010.5,
            ),
        )
        assert convert_target(beam) == expected

    def test_beam_conversion_strips_defaults(self):
        """
        Verify that default values in a PDM Beam are stripped from the output
        CDM object.
        """
        beam = Beam(
            beam_id=1,
            beam_coordinate=EquatorialCoordinatesPST(
                target_id="foo",
                reference_frame=EquatorialCoordinatesReferenceFrame.ICRS,
                ra_str="01:20:00.00",
                dec_str="-45:30:00",
            ),
        )
        expected = ICRSField(
            target_name="foo",
            attrs=ICRSField.Attrs(
                # astropy doesn't convert cleanly
                # c1=20.0,
                c1=19.999999999999996,
                c2=-45.5,
                epoch=2000.0,
            ),
        )
        assert convert_target(beam) == expected

    def test_icrs_target_conversion(self):
        """
        Test that a PDM ICRS Target is converted to the expected CDM
        ICRSField.
        """
        pdm_target = Target(
            target_id="foo",
            reference_coordinate=EquatorialCoordinates(
                ra="01:20:00.00", dec="-45:30:00"
            ),
            radial_velocity=RadialVelocity(
                quantity=Quantity(1.23, unit=RadialVelocityUnits.KM_PER_SEC)
            ),
        )
        expected = ICRSField(
            target_name="foo",
            attrs=ICRSField.Attrs(
                # astropy doesn't convert cleanly
                # c1=20.0,
                c1=19.999999999999996,
                c2=-45.5,
                radial_velocity=1230.0,
            ),
        )
        assert convert_target(pdm_target) == expected

    def test_azel_target_conversion(self):
        """
        Test that a PDM Az/Alt target is converted to the expected CDM
        AzAltField.
        """
        pdm_target = Target(
            target_id="foo",
            reference_coordinate=HorizontalCoordinates(az=1.23, el=4.56),
            radial_velocity=RadialVelocity(
                quantity=Quantity(1.23, unit=RadialVelocityUnits.KM_PER_SEC)
            ),
        )
        expected = AltAzField(
            target_name="foo",
            attrs=AltAzField.Attrs(
                c1=1.23,
                c2=4.56,
            ),
        )
        assert convert_target(pdm_target) == expected

    def test_solar_system_target_conversion(self):
        """
        Test that a PDM Solar System target is converted to the expected CDM
        SpecialField.
        """
        pdm_target = Target(
            target_id="foo",
            reference_coordinate=SolarSystemObject(name=SolarSystemObjectName.MARS),
            radial_velocity=RadialVelocity(
                quantity=Quantity(1.23, unit=RadialVelocityUnits.KM_PER_SEC)
            ),
        )
        expected = SpecialField(target_name=cdm_SSO.MARS)
        assert convert_target(pdm_target) == expected
