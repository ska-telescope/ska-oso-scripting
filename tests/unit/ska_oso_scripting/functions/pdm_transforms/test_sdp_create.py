"""
Unit tests for the ska_oso_scripting.pdm_transforms.sdp module's
SDP creation functions.

The purpose of these tests is to confirm that the pdm_transforms code creates
CDM entities for SDP as expected based on the Scheduling Block data.
"""
import copy
import os
import unittest
from datetime import timedelta
from unittest import mock

import pytest
from freezegun import freeze_time
from ska_oso_pdm import TelescopeType
from ska_oso_pdm.builders import low_imaging_sb, mid_imaging_sb
from ska_oso_pdm.sb_definition import (
    AltAzCoordinates,
    EquatorialCoordinates,
    EquatorialCoordinatesReferenceFrame,
    FivePointParameters,
    GalacticCoordinates,
    PointingPattern,
    RasterParameters,
    ScanDefinition,
    SinglePointParameters,
    SolarSystemObject,
    SolarSystemObjectName,
    StarRasterParameters,
    Target,
)
from ska_ser_skuid.client import SkuidClient
from ska_tmc_cdm.messages.central_node.sdp import Channel as cdm_Channel
from ska_tmc_cdm.messages.central_node.sdp import (
    ChannelConfiguration as cdm_ChannelConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import EBScanType as cdm_EBScanType
from ska_tmc_cdm.messages.central_node.sdp import EBScanTypeBeam
from ska_tmc_cdm.messages.central_node.sdp import (
    ExecutionBlockConfiguration as cdm_EBConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import (
    FieldConfiguration as cdm_FieldConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import PbDependency as cdm_PbDependency
from ska_tmc_cdm.messages.central_node.sdp import PhaseDir as cdm_PhaseDir
from ska_tmc_cdm.messages.central_node.sdp import PolarisationConfiguration
from ska_tmc_cdm.messages.central_node.sdp import (
    ProcessingBlockConfiguration as cdm_ProcessingBlockConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import (
    ScriptConfiguration as cdm_ScriptConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import (
    SDPConfiguration as cdm_centralnode_SDPConfiguration,
)
from ska_tmc_cdm.messages.subarray_node.configure.sdp import (
    SDPConfiguration as cdm_subarraynode_SDPConfiguration,
)

from ska_oso_scripting.functions.pdm_transforms.sdp import (
    create_sdpconfiguration_subarraynode,
)
from ska_oso_scripting.functions.pdm_transforms.sdp_create import (
    FIVE_POINT_SCANTYPE_PREFIX,
    TargetConversion,
    convert_target_to_fieldconfiguration,
    create_processing_blocks,
    create_resources,
    create_sdpconfiguration_centralnode,
    create_spws,
    low_processing_block_parameters_dict,
    mid_processing_block_five_point_parameters_dict,
    mid_processing_block_parameters_dict,
)
from tests.unit.ska_oso_scripting.builders.cdm.sdp.beamconfiguration import (
    BeamConfigurationBuilder,
)

MID_SB = mid_imaging_sb()
LOW_SB = low_imaging_sb()


class TestTargetConversion:
    SINGLE_POINTING_NO_OFFSET = Target(
        target_id="sidereal target 1",
        pointing_pattern=PointingPattern(
            active=SinglePointParameters().kind,
            parameters=[
                SinglePointParameters(offset_x_arcsec=0.0, offset_y_arcsec=0.0)
            ],
        ),
        reference_coordinate=EquatorialCoordinates(
            ra="12:34:56.78",
            dec="+12:34:56.78",
            reference_frame=EquatorialCoordinatesReferenceFrame.ICRS,
        ),
    )
    CONVERTED_SINGLE_POINTING_NO_OFFSET = cdm_FieldConfiguration(
        field_id="sidereal target 1",
        pointing_fqdn=None,
        phase_dir=cdm_PhaseDir(
            ra=[188.73658333333333],
            dec=[12.582438888888891],
            reference_time="2023-02-16T01:23:45+00:00",
            reference_frame="ICRF3",
        ),
    )

    GALACTIC_SINGLE_POINTING_NO_OFFSET = Target(
        target_id="Crab Nebular",
        pointing_pattern=PointingPattern(
            active=SinglePointParameters().kind,
            parameters=[
                SinglePointParameters(offset_x_arcsec=0.0, offset_y_arcsec=0.0)
            ],
        ),
        reference_coordinate=GalacticCoordinates(
            l=184.5547,
            b=-05.7833,
        ),
    )

    CONVERTED_GALACTIC_SINGLE_POINTING_NO_OFFSET = cdm_FieldConfiguration(
        field_id="Crab Nebular",
        pointing_fqdn=None,
        phase_dir=cdm_PhaseDir(
            ra=[83.6324617],
            dec=[22.01738325],
            reference_time="2023-02-16T01:23:45+00:00",
            reference_frame="ICRF3",
        ),
    )

    FIVE_POINT_CALIBRATION = Target(
        target_id="Calibrator Target",
        pointing_pattern=PointingPattern(
            active=FivePointParameters().kind,
            parameters=[FivePointParameters(offset_arcsec=5.0)],
        ),
        reference_coordinate=EquatorialCoordinates(
            ra="12:34:56.78",
            dec="+12:34:56.78",
            reference_frame=EquatorialCoordinatesReferenceFrame.ICRS,
        ),
    )
    CONVERTED_FIVE_POINT_CALIBRATION = cdm_FieldConfiguration(
        field_id="Calibrator Target",
        pointing_fqdn=None,
        phase_dir=cdm_PhaseDir(
            ra=[188.73658333333333],
            dec=[12.582438888888891],
            reference_time="2023-02-16T01:23:45+00:00",
            reference_frame="ICRF3",
        ),
    )

    SINGLE_POINTING_WITH_OFFSET = Target(
        target_id="sidereal target 2",
        pointing_pattern=PointingPattern(
            active=SinglePointParameters().kind,
            parameters=[
                SinglePointParameters(offset_x_arcsec=0.1, offset_y_arcsec=0.1)
            ],
        ),
        reference_coordinate=EquatorialCoordinates(
            ra="01:23:45.67",
            dec="-12:34:56.78",
            reference_frame=EquatorialCoordinatesReferenceFrame.ICRS,
        ),
    )
    CONVERTED_SINGLE_POINTING_WITH_OFFSET = cdm_FieldConfiguration(
        field_id="sidereal target 2",
        pointing_fqdn=None,
        phase_dir=cdm_PhaseDir(
            ra=[20.94032012798039],
            dec=[-12.582411111109609],
            reference_time="2023-02-16T01:23:45+00:00",
            reference_frame="ICRF3",
        ),
    )

    @pytest.mark.parametrize(
        "pdm_instance,expected",
        [
            (SINGLE_POINTING_NO_OFFSET, CONVERTED_SINGLE_POINTING_NO_OFFSET),
            (
                SINGLE_POINTING_WITH_OFFSET,
                CONVERTED_SINGLE_POINTING_WITH_OFFSET,
            ),
            (
                GALACTIC_SINGLE_POINTING_NO_OFFSET,
                CONVERTED_GALACTIC_SINGLE_POINTING_NO_OFFSET,
            ),
            (FIVE_POINT_CALIBRATION, CONVERTED_FIVE_POINT_CALIBRATION),
        ],
    )
    @freeze_time("2023-02-16T01:23:45Z")
    def test_convert_target_to_fieldconfiguration(self, pdm_instance, expected):
        converted = TargetConversion.convert_target_to_fieldconfiguration(pdm_instance)
        assert converted == expected

    @pytest.mark.parametrize(
        "pattern_param_cls",
        [StarRasterParameters, RasterParameters],
    )
    def test_convert_target_raises_exception_on_unsupported_pointing_pattern(
        self, pattern_param_cls
    ):
        """
        Confirm that an exception is raised if an unsupported pointing pattern
        is requested.
        """
        pattern_param = pattern_param_cls()
        unsupported_target = Target(
            target_id="target ID",
            pointing_pattern=PointingPattern(
                active=pattern_param.kind, parameters=[pattern_param]
            ),
            reference_coordinate=EquatorialCoordinates(),
        )

        with pytest.raises(NotImplementedError):
            TargetConversion.convert_target_to_fieldconfiguration(unsupported_target)

    def test_exception_raised_for_untransformable_coords(self):
        """
        Verify that converting anything but an ra/dec coord results in an exception.
        """
        copied = copy.deepcopy(self.SINGLE_POINTING_NO_OFFSET)
        copied.reference_coordinate = AltAzCoordinates(az=0.0, el=45.0)
        with pytest.raises(NotImplementedError):
            convert_target_to_fieldconfiguration(copied)

    @freeze_time("2023-02-16T01:23:45Z")
    def test_erroneous_special_object_result(self):
        """
        Verify that special objects return obviously erroneous result.
        """
        pdm_target = Target(
            target_id="SSO",
            reference_coordinate=SolarSystemObject(name=SolarSystemObjectName.MARS),
        )

        expected = cdm_FieldConfiguration(
            field_id="SSO",
            pointing_fqdn=None,
            phase_dir=cdm_PhaseDir(
                ra=[0.0],
                dec=[-90.0],
                reference_time="2023-02-16T01:23:45+00:00",
                reference_frame="ICRF3",
            ),
        )

        skycoord = convert_target_to_fieldconfiguration(pdm_target)

        assert skycoord == expected


class SDPCreateForSubarrayNodeTests(unittest.TestCase):
    """
    Unit tests for SDPConfiguration PDM to CDM transforms.
    """

    @staticmethod
    def test_create_sdpconfiguration_subarraynode_creates_expected_cdm_entity():
        scan_definition_id = "science_A"
        sample_scan_def = ScanDefinition(
            scan_definition_id=scan_definition_id,
            scan_duration_ms=timedelta(seconds=12),
            target_ref="typical",
            dish_allocation_ref="dish config 123",
        )
        expected = cdm_subarraynode_SDPConfiguration(scan_type=scan_definition_id)
        actual = create_sdpconfiguration_subarraynode(sample_scan_def, Target())
        assert actual == expected

    @staticmethod
    def test_create_sdpconfiguration_subarraynode_creates_expected_cdm_entity_for_five_point():
        """
        Verify that create_sdpconfiguration_subarraynode adds the pointing prefix for SDP when
        the target is a five-point target.
        """
        scan_definition_id = "science_A"
        sample_scan_def = ScanDefinition(
            scan_definition_id=scan_definition_id,
            scan_duration_ms=timedelta(seconds=12),
            target_ref="typical",
            dish_configuration_ref="dish config 123",
        )
        expected = cdm_subarraynode_SDPConfiguration(
            scan_type=FIVE_POINT_SCANTYPE_PREFIX + scan_definition_id
        )
        actual = create_sdpconfiguration_subarraynode(
            sample_scan_def, TestTargetConversion.FIVE_POINT_CALIBRATION
        )
        assert actual == expected

    @staticmethod
    def test_create_sdpconfiguration_subarraynode_complains_on_invalid_type():
        """
        Verify that create_sdpconfiguration_subarraynode checks input argument type.
        """
        with pytest.raises(TypeError):
            create_sdpconfiguration_subarraynode(1, 2)


class TestSpectralWindowCreation:
    EXPECTED_LOW_SPW = cdm_Channel(
        spectral_window_id="vis_spw_1",
        count=96 * 144,
        start=0,
        stride=1,
        freq_min=162109375,  # These values are based on expected centre frequency of 199609375.0 in LOW_SB
        freq_max=237109375,
    )
    EXPECTED_MID_SPW = cdm_Channel(
        spectral_window_id="vis_spw_1",
        count=14880,
        start=0,
        stride=1,
        freq_min=350013440,
        freq_max=550000640,
    )

    def test_create_spws_low(self):
        low_csp_input = copy.deepcopy(LOW_SB.csp_configurations[0])
        expected_spws = [self.EXPECTED_LOW_SPW]
        actual_spws = create_spws(low_csp_input)
        assert actual_spws == expected_spws

    @pytest.mark.parametrize(
        "zoom_factor, expected_min_max_freq",
        (
            (0, (EXPECTED_MID_SPW.freq_min, EXPECTED_MID_SPW.freq_max)),
            (1, (400010240, 500003840)),
            (2, (425008640, 475005440)),
        ),
    )  # These tests are based on expected centre frequency of 450007040.0 in MID_SB
    def test_create_spws_mid(self, zoom_factor, expected_min_max_freq):
        mid_csp_input = copy.deepcopy(MID_SB.csp_configurations[0])
        mid_csp_input.midcbf.subbands[0].correlation_spws[0].zoom_factor = zoom_factor
        expected_spw = copy.deepcopy(self.EXPECTED_MID_SPW)
        expected_spw.freq_min = expected_min_max_freq[0]
        expected_spw.freq_max = expected_min_max_freq[1]
        actual_spws = create_spws(mid_csp_input)
        assert actual_spws == [expected_spw]


class TestProcessingBlockCreation:
    EXPECTED_PB_ID = "pb-1"
    EXPECTED_PB_ID2 = "pb-2"
    EXPECTED_LOW_PB = cdm_ProcessingBlockConfiguration(
        pb_id=EXPECTED_PB_ID,
        sbi_ids=[],
        script=cdm_ScriptConfiguration(
            kind="realtime", name="vis-receive", version="4.5.0"
        ),
        parameters=low_processing_block_parameters_dict,
    )

    EXPECTED_MID_PB = cdm_ProcessingBlockConfiguration(
        pb_id=EXPECTED_PB_ID,
        sbi_ids=[],
        script=cdm_ScriptConfiguration(
            kind="realtime", name="vis-receive", version="4.5.0"
        ),
        parameters=mid_processing_block_parameters_dict,
    )

    EXPECTED_MID_FIVE_POINT_PBS = [
        cdm_ProcessingBlockConfiguration(
            pb_id=EXPECTED_PB_ID,
            sbi_ids=[],
            script=cdm_ScriptConfiguration(
                kind="realtime", name="vis-receive", version="4.5.0"
            ),
            parameters=mid_processing_block_parameters_dict,
            dependencies=[
                cdm_PbDependency(pb_id=EXPECTED_PB_ID2, kind=["pointing-offset"])
            ],
        ),
        cdm_ProcessingBlockConfiguration(
            pb_id=EXPECTED_PB_ID2,
            sbi_ids=[],
            script=cdm_ScriptConfiguration(
                kind="realtime", name="pointing-offset", version="0.8.0"
            ),
            parameters=mid_processing_block_five_point_parameters_dict,
            dependencies=[cdm_PbDependency(pb_id=EXPECTED_PB_ID, kind=["vis-receive"])],
        ),
    ]

    @mock.patch.object(SkuidClient, "fetch_skuid", scope="class")
    def test_create_pb_low(self, mock_fetch_fn, mock_settings_env_vars):
        mock_fetch_fn.side_effect = [self.EXPECTED_PB_ID]

        actual_pbs = create_processing_blocks(TelescopeType.SKA_LOW, LOW_SB.targets)
        assert len(actual_pbs) == 1
        assert actual_pbs[0] == self.EXPECTED_LOW_PB

    @mock.patch.object(SkuidClient, "fetch_skuid", scope="class")
    def test_create_pb_mid(self, mock_fetch_fn, mock_settings_env_vars):
        mock_fetch_fn.side_effect = [self.EXPECTED_PB_ID]

        actual_pbs = create_processing_blocks(TelescopeType.SKA_MID, MID_SB.targets)
        assert len(actual_pbs) == 1
        assert actual_pbs[0] == self.EXPECTED_MID_PB

    @mock.patch.object(SkuidClient, "fetch_skuid", scope="class")
    def test_create_pb_mid_five_point(self, mock_fetch_fn, mock_settings_env_vars):
        mock_fetch_fn.side_effect = [self.EXPECTED_PB_ID, self.EXPECTED_PB_ID2]

        actual_pbs = create_processing_blocks(
            TelescopeType.SKA_MID,
            [MID_SB.targets[0], TestTargetConversion.FIVE_POINT_CALIBRATION],
        )
        assert len(actual_pbs) == 2
        assert actual_pbs == self.EXPECTED_MID_FIVE_POINT_PBS


class TestResourceCreation:
    EXPECTED_MID_RESOURCES = {"receptors": ["SKA001", "SKA036", "SKA063", "SKA100"]}
    EXPECTED_LOW_RESOURCES = {"receptors": ["S8-1", "S8-6", "S9-2", "S10-3"]}

    def test_create_resources_mid(self):
        actual_resources = create_resources(MID_SB)
        assert actual_resources == self.EXPECTED_MID_RESOURCES

    def test_create_resources_low(self):
        actual_resources = create_resources(LOW_SB)
        assert actual_resources == self.EXPECTED_LOW_RESOURCES


@freeze_time("2023-02-16T01:23:45Z")
@mock.patch.dict(os.environ, {"EB_ID": "eb-test-00000000-99999"})
class TestSDPCreateForCentralNode:
    """
    Tests to confirm that CDM SDPConfiguration object is created as expected from the
    Scheduling Block Definition.
    """

    EXPECTED_MID_EB = cdm_EBConfiguration(
        eb_id="eb-test-00000000-99999",
        max_length=1000,
        beams=[
            BeamConfigurationBuilder().for_visibilities().with_beam_id("vis0").build()
        ],
        channels=[
            cdm_ChannelConfiguration(
                channels_id="vis_channels",
                spectral_windows=[TestSpectralWindowCreation.EXPECTED_MID_SPW],
            )
        ],
        fields=[
            convert_target_to_fieldconfiguration(target) for target in MID_SB.targets
        ],
        polarisations=[
            PolarisationConfiguration(
                polarisations_id="all", corr_type=["XX", "XY", "YX", "YY"]
            )
        ],
        scan_types=[
            cdm_EBScanType(
                scan_type_id=MID_SB.scan_definitions[0].scan_definition_id,
                beams={
                    "vis0": EBScanTypeBeam(
                        field_id=MID_SB.scan_definitions[0].target_ref,
                        channels_id="vis_channels",
                        polarisations_id="all",
                    )
                },
            )
        ],
    )
    # Hacky fix for freeze_time not working in the constructor
    for field in EXPECTED_MID_EB.fields:
        field.phase_dir.reference_time = "2023-02-16T01:23:45+00:00"

    EXPECTED_MID_SDP = cdm_centralnode_SDPConfiguration(
        execution_block=EXPECTED_MID_EB,
        resources=TestResourceCreation.EXPECTED_MID_RESOURCES,
        processing_blocks=[TestProcessingBlockCreation.EXPECTED_MID_PB],
    )

    @mock.patch.object(SkuidClient, "fetch_skuid", scope="class")
    def test_create_sdpconfiguration_from_mid_sb(
        self, mock_fetch_fn, mock_settings_env_vars
    ):
        mock_fetch_fn.side_effect = [TestProcessingBlockCreation.EXPECTED_PB_ID]
        actual_sdp = create_sdpconfiguration_centralnode(MID_SB)
        assert actual_sdp == self.EXPECTED_MID_SDP

    @mock.patch.object(SkuidClient, "fetch_skuid", scope="class")
    def test_create_sdpconfiguration_from_mid_sb_with_five_point(
        self, mock_fetch_fn, mock_settings_env_vars
    ):
        mock_fetch_fn.side_effect = [
            TestProcessingBlockCreation.EXPECTED_PB_ID,
            TestProcessingBlockCreation.EXPECTED_PB_ID2,
        ]

        five_point_mid_sb = copy.deepcopy(MID_SB)
        five_point_mid_sb.targets = [TestTargetConversion.FIVE_POINT_CALIBRATION]
        five_point_mid_sb.scan_definitions[
            0
        ].target_ref = TestTargetConversion.FIVE_POINT_CALIBRATION.target_id

        expected_sdp = copy.deepcopy(self.EXPECTED_MID_SDP)
        expected_sdp.execution_block.fields[
            0
        ] = TestTargetConversion.CONVERTED_FIVE_POINT_CALIBRATION
        expected_sdp.execution_block.scan_types[0].scan_type_id = (
            FIVE_POINT_SCANTYPE_PREFIX
            + five_point_mid_sb.scan_definitions[0].scan_definition_id
        )
        expected_sdp.execution_block.scan_types[0].beams[
            "vis0"
        ].field_id = TestTargetConversion.FIVE_POINT_CALIBRATION.target_id
        expected_sdp.processing_blocks = (
            TestProcessingBlockCreation.EXPECTED_MID_FIVE_POINT_PBS
        )

        actual_sdp = create_sdpconfiguration_centralnode(five_point_mid_sb)
        assert actual_sdp == expected_sdp

    @mock.patch.object(SkuidClient, "fetch_skuid", scope="class")
    def test_create_sdpconfiguration_from_low_sb(
        self, mock_fetch_fn, mock_settings_env_vars
    ):
        expected_low_eb = cdm_EBConfiguration(
            eb_id="eb-test-00000000-99999",
            max_length=1000,
            beams=[
                BeamConfigurationBuilder()
                .for_visibilities()
                .with_beam_id("vis0")
                .build()
            ],
            channels=[
                cdm_ChannelConfiguration(
                    channels_id="vis_channels",
                    spectral_windows=[TestSpectralWindowCreation.EXPECTED_LOW_SPW],
                )
            ],
            fields=[
                convert_target_to_fieldconfiguration(target)
                for target in LOW_SB.targets
            ],
            polarisations=[
                PolarisationConfiguration(
                    polarisations_id="all", corr_type=["XX", "XY", "YX", "YY"]
                )
            ],
            scan_types=[
                cdm_EBScanType(
                    scan_type_id=LOW_SB.scan_definitions[0].scan_definition_id,
                    beams={
                        "vis0": EBScanTypeBeam(
                            field_id=LOW_SB.scan_definitions[0].target_ref,
                            channels_id="vis_channels",
                            polarisations_id="all",
                        )
                    },
                )
            ],
        )

        expected_low_sdp = cdm_centralnode_SDPConfiguration(
            execution_block=expected_low_eb,
            resources=TestResourceCreation.EXPECTED_LOW_RESOURCES,
            processing_blocks=[TestProcessingBlockCreation.EXPECTED_LOW_PB],
        )

        mock_fetch_fn.side_effect = [TestProcessingBlockCreation.EXPECTED_PB_ID]

        actual_sdp = create_sdpconfiguration_centralnode(LOW_SB)
        assert actual_sdp == expected_low_sdp
