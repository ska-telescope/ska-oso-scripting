"""
Verify we can run scripts again a TMC simulator.
"""
import dataclasses
import importlib
import json
import re
from functools import partial
from typing import Optional

import number_parser
import pytest
from dotwiz import DotWiz
from pytest_bdd import given, parsers, scenarios, then, when
from ska_control_model import ObsState
from ska_oso_pdm import TelescopeType
from ska_oso_tmc_integration_tests.tmcsim.subarraynode import MethodCall

from ska_oso_scripting import WORKAROUNDS
from ska_oso_scripting.functions.devicecontrol.common import ValueTransitionError
from tests.integration.conftest import EventsSummary, event_recorder
from tests.integration.resources.test_harness import OSOTestHarness

scenarios(
    "pointing_calibration.feature",
    "special_transforms.feature",
    "sad_paths.feature",
    "adr63_verification.feature",
    "adr106_verification.feature",
)

# Disable the trajectory workaround for tests to allow testing of pointing groups
# (issue only relevant when running on real TMC)
WORKAROUNDS.disable_pointing_groups = False
# Disable workaround for pausing before reconfigure for quicker test execution
# (issue only relevant when real CSP is present)
WORKAROUNDS.pause_before_reconfigure = False


@dataclasses.dataclass
class TestContext:
    events: EventsSummary
    command_history: list
    error: Optional[ValueTransitionError] = None


@given(
    parsers.parse("a telescope control system for {telescope}"),
    target_fixture="test_harness",
)
def new_test_harness(telescope: str):
    test_harness = OSOTestHarness(TelescopeType(telescope))
    test_harness.add_central_node()
    return test_harness


@given(parsers.parse("{obsstate} subarray {subarray_id:d}"))
def add_subarray_with_obsstate(test_harness, obsstate, subarray_id):
    initial_obsstate = getattr(ObsState, obsstate)
    test_harness.add_subarray(subarray_id, initial_obsstate=initial_obsstate)


@given(parsers.parse("subarray {subarray_id:d} goes to FAULT state after {states}"))
def set_up_fault_injection(test_harness, subarray_id, states):
    test_harness.add_subarray_fault_injection(subarray_id, f"[{states}]")


@when(
    parsers.parse("I run {script} on subarray {subarray_id:d} with SBD {sbd}"),
    target_fixture="ctx",
)
def run_observing_script(test_harness, script, subarray_id, sbd, constants):
    san_fqdn = f"{test_harness.telescope}/tm_subarray_node/{subarray_id}"

    script_module = importlib.import_module(f"scripts.{script}")
    with test_harness as ctx:
        recorder = partial(event_recorder, ctx, san_fqdn)
        san_device = ctx.get_device(san_fqdn)
        if subarray_id in test_harness.fail_after:
            states_str = test_harness.fail_after[subarray_id]
            san_device.InjectFaultAfter(states_str)
        error = None
        with recorder() as events:
            try:
                script_module.init(subarray_id)
                script_module.main(
                    sbi_id="test_SBI", sb_json=constants["DATA_DIR"] / sbd
                )
            except Exception as err:
                error = err
            summary = EventsSummary(events)

        history = [MethodCall.model_validate(s) for s in json.loads(san_device.History)]

        results = TestContext(events=summary, command_history=history, error=error)

    return results


@then(parsers.parse("the subarray obsState passes through {obsstate} {count:d} times"))
def assert_obsstate_count(ctx: TestContext, obsstate, count):
    obsstate_enum = ObsState[obsstate]
    assert ctx.events.obsstate_counts[obsstate_enum] == count


@then(parsers.parse("the final obsState is {obsstate}"))
def assert_final_obsstate(ctx: TestContext, obsstate):
    obsstate_enum = ObsState[obsstate]
    assert ctx.events.final_obsstate == obsstate_enum


@then(
    parsers.parse(
        "the {ordinal} {command} JSON payload has a {json_key} value equal to {expected}"
    )
)
def assert_json_includes_value(ctx: TestContext, ordinal, command, json_key, expected):
    dotted_json = generate_dotted_json(ctx, ordinal, command)
    actual = get_value(dotted_json, json_key)
    # cast to string to avoid float/str comparison errors
    assert str(actual) == expected


@then(parsers.parse("the {ordinal} {command} JSON payload contains {json_key}"))
def assert_key_is_present_in_json(ctx: TestContext, ordinal, command, json_key):
    dotted_json = generate_dotted_json(ctx, ordinal, command)
    actual = get_value(dotted_json, json_key)

    assert actual is not None


@then(parsers.parse("the {ordinal} {command} JSON payload does not contain {json_key}"))
def assert_json_excludes(ctx: TestContext, ordinal, command, json_key):
    try:
        assert_key_is_present_in_json(
            ctx=ctx, ordinal=ordinal, command=command, json_key=json_key
        )
    except KeyError:
        # Should be raised by get_value() if the JSON key is missing, e.g.,
        # KeyError: 'correction'
        pass
    else:
        pytest.fail(f"{json_key} found in {ordinal} {command} command")


@then(
    parsers.parse(
        "the script raises a ValueTransitionError as subarray {subarray_id:d} transitioned to {received_state} instead of {expected_state}"
    )
)
def assert_script_raises_transition_error(
    ctx: TestContext, subarray_id: int, received_state: str, expected_state: str
):
    assert ctx.error is not None
    assert isinstance(ctx.error, ValueTransitionError)

    # Convert the string state to the int
    received_state = getattr(ObsState, received_state).value
    expected_state = getattr(ObsState, expected_state).value

    assert (
        f"""Expected <Attribute("ska_mid/tm_subarray_node/{subarray_id}", "obsState")> to transition to {expected_state} but instead received {received_state}"""
        in str(ctx.error)
    )


def generate_dotted_json(ctx: TestContext, ordinal, command):
    """ "
    this code generates the dot-notation json
    """
    calls_for_cmd = [c for c in ctx.command_history if c.command == command]

    idx = number_parser.parse_ordinal(ordinal)
    try:
        cmd_history = calls_for_cmd[idx - 1]
    except IndexError as e:
        raise IndexError(
            "{ordinal} {command} history not found; {len(cmd_history)} {command} commands were recorded"
        ) from e

    json_payload = json.loads(cmd_history.args[0])
    return DotWiz(json_payload)


def get_value(d, path):
    """
    Retrieve a value from a nested dictionary/list structure
    using a dot-notation path with list indices.

    Example:
        d = {'foo': {'bar': [0, {'baz': {'bee': [10, 20, 30]}}]}}
        path = 'foo.bar[1].baz.bee[2]'
        get_value(d, path)  # returns 30
    """
    tokens = re.split(r"\.", path)
    current = d
    for token in tokens:
        # Find dictionary keys and list indices.
        parts = re.findall(r"([^\[\]]+)|\[(\d+)]", token)
        for key, index in parts:
            if key:  # dictionary key found
                current = current[key]
            elif index:  # list index found
                current = current[int(index)]
    return current
