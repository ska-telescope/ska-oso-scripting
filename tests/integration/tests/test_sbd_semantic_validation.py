import os
from unittest import mock

from pytest_bdd import given, parsers, scenarios, then, when
from ska_ost_osd.telvalidation.schematic_validation_exceptions import (
    SchematicValidationError,
)
from ska_tmc_cdm.messages.central_node.assign_resources import AssignResourcesRequest
from ska_tmc_cdm.messages.subarray_node.configure import ConfigureRequest

from ska_oso_scripting.functions.devicecontrol.common import get_request_json
from ska_oso_scripting.functions.pdm_transforms import (
    create_cdm_assign_resources_request_from_scheduling_block,
    create_cdm_configure_request_from_scheduling_block,
)

scenarios("sbd_semantic_validate.feature")


@given(
    parsers.parse("a telescope control system for {telescope}"),
    target_fixture="telescope",
)
def set_telescope_environment(telescope: str):
    """
    Setting the environment variable
    """
    if telescope == "ska_low":
        tel = telescope.split("_")
        os.environ["SKA_TELESCOPE"] = str(tel[0].upper() + "-" + tel[1])


@given(
    parsers.parse("an invalid SB JSON {invalid_sbd_json}"),
    target_fixture="semantically_invalid_sbd",
)
def semantically_invalid_sbd(invalid_sbd_json, create_sbd_from_json_file):
    """
     Given step to generate a semantically invalid Scheduling Block Definition (SBD).

    :param invalid_sbd_json: A fixture that provides the path to the invalid SBD JSON file.
    :param create_sbd_from_json_file: A fixture that creates an SBD instance from the provided JSON file.
    :return: An instance of Scheduling Block that is semantically invalid.
    """
    return create_sbd_from_json_file(invalid_sbd_json)


@when(
    "I generate the CDM Assign and Configure request for observing command using an invalid SBD",
    target_fixture="validation_error_messages",
)
@mock.patch.dict(os.environ, {"EB_ID": "eb-test-00000000-99999"})
def created_request_cdm_object(semantically_invalid_sbd):
    """
    When step to generate CDM Assign and Configure request from a semantically invalid SBD.

    :param semantically_invalid_sbd: An instance of Scheduling Block that is semantically invalid.
    :return: A dictionary of error messages caught for CDM Assign and CDM Configure requests.
    """
    validation_error_messages = []

    cdm_assign_request = create_cdm_assign_resources_request_from_scheduling_block(
        1, semantically_invalid_sbd
    )
    try:
        get_request_json(cdm_assign_request, AssignResourcesRequest)
    except SchematicValidationError as error:
        validation_error_messages.append(error)

    cdm_configure_resource = create_cdm_configure_request_from_scheduling_block(
        semantically_invalid_sbd
    )["science scan"][0]
    try:
        get_request_json(cdm_configure_resource, ConfigureRequest)
    except SchematicValidationError as error:
        validation_error_messages.append(error)

    return validation_error_messages


@then("semantic validation framework should raise an error for all the invalid values")
def check_error_raised(validation_error_messages):
    """
    Then step to check if any of the semantic invalid values are raised.

    :param validation_error_messages: A dictionary of error messages caught for CDM Assign and CDM Configure requests.
    :return: None
    """
    assert isinstance(validation_error_messages[0], SchematicValidationError)
