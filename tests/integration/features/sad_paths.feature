Feature: Handle different sad paths when executing an SB.

  @XTP-59579 @XTP-57209 @XTP-57212
  Scenario Outline: Inject a fault after configure and ensure the script handles it properly.
    Given a telescope control system for <telescope>
    And EMPTY subarray 1
    And subarray 1 goes to FAULT state after <fault_after>
    When I run <script> on subarray 1 with SBD <sbd>
    Then the subarray obsState passes through CONFIGURING <num_configuring_expected> times
    And the subarray obsState passes through SCANNING <num_scanning_expected> times
    And the script raises a ValueTransitionError as subarray 1 transitioned to FAULT instead of <expected_state_before_fault>
    And the final obsState is FAULT

  Examples:
    | script                     | sbd                               | telescope | fault_after            | num_configuring_expected  | num_scanning_expected | expected_state_before_fault |
    | allocate_and_observe_sb    | testfile_nonsidereal_sb_mid.json  | ska_mid   | 'IDLE', 'CONFIGURING'  | 1                         | 0                     | READY                       |
    | allocate_and_observe_sb    | testfile_nonsidereal_sb_mid.json  | ska_mid   | 'SCANNING'             | 1                         | 1                     | READY                       |