Feature: Carry out a scan that involves special transforms.
  @XTP-57901 @XTP-57209 @XTP-57212
  Scenario Outline: Run an SB containing a 'special' non-sidereal target.
    Given a telescope control system for <telescope>
    And EMPTY subarray 1
    When I run <script> on subarray 1 with SBD <sbd>
    Then the subarray obsState passes through CONFIGURING <num_scans_expected> times
    And the subarray obsState passes through SCANNING <num_scans_expected> times
    And the final obsState is EMPTY


  Examples:
    | script                     | sbd                               | telescope | num_scans_expected |
    | allocate_and_observe_sb    | testfile_nonsidereal_sb_mid.json  | ska_mid   | 3                  |