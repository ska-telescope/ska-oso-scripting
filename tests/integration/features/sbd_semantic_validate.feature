Feature: Semantic Validation for SBD

  @XTP-57900 @XTP-57209 @XTP-57212
  Scenario Outline: Semantically validating Scheduling Block Definition
    Given a telescope control system for <telescope>
    And an invalid SB JSON <invalid_sbd_json>
    When I generate the CDM Assign and Configure request for observing command using an invalid SBD
    Then semantic validation framework should raise an error for all the invalid values

    Examples:
    | invalid_sbd_json              | telescope |
    | testfile_invalid_sb_mid.json  | ska_mid   |
    | testfile_invalid_sb_low.json  | ska_low   |
