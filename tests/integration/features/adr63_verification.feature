Feature: Run scripts for various scheduling blocks.

  @XTP-57209 @XTP-57212 @XTP-76369
  Scenario Outline: OSO sends target coordinates in SKA-standard ADR-63 format and, for backwards compatibility, pre-ADR-63 form
    Given a telescope control system for <telescope>
    And EMPTY subarray 1
    When I run <script> on subarray 1 with SBD <sbd>

    # Both of the following SBs should generate identical Configure requests with both pointing.target
    # and pointing.groups populated

    Then the first Configure JSON payload has a pointing.target.ra value equal to 16:51:07.98866878
    And the first Configure JSON payload has a pointing.target.dec value equal to +04:59:35.54713147
    And the first Configure JSON payload has a pointing.target.reference_frame value equal to ICRS
    And the first Configure JSON payload has a pointing.groups[0].field.reference_frame value equal to icrs
    And the first Configure JSON payload has a pointing.groups[0].field.attrs.c1 value equal to 252.78328611990997
    And the first Configure JSON payload has a pointing.groups[0].field.attrs.c2 value equal to 4.99320753652
    And the first Configure JSON payload has a pointing.groups[0].field.attrs.epoch value equal to 2000.0
    And the first Configure JSON payload has a pointing.groups[0].field.attrs.radial_velocity value equal to 46437852.0
    And the first Configure JSON payload has a pointing.groups[0].trajectory.name value equal to fixed
    And the first Configure JSON payload has a pointing.groups[0].trajectory.attrs.x value equal to -5.0
    And the first Configure JSON payload has a pointing.groups[0].trajectory.attrs.y value equal to 5.0
    And the first Configure JSON payload has a pointing.groups[0].projection.name value equal to SIN
    And the first Configure JSON payload has a pointing.groups[0].projection.alignment value equal to ICRS

    Examples:
    | script                     | sbd                                     | telescope |
    | allocate_and_observe_sb    | testfile_pre_ADR-63_imaging_sb_mid.json | ska_mid   |
    | allocate_and_observe_sb    | testfile_imaging_sb_mid.json            | ska_mid   |