Feature: Run scripts for various scheduling blocks.

  @XTP-57209 @XTP-57212 @XTP-77013
  Scenario: OSO can support a MID observation of a pointing mosiac
    Given a telescope control system for ska_mid
    And EMPTY subarray 1
    When I run allocate_and_observe_sb on subarray 1 with SBD testfile_mosaic_sb_mid.json
    Then the subarray obsState passes through CONFIGURING 3 times
    And the subarray obsState passes through SCANNING 3 times
    And the final obsState is EMPTY
    # Only the first configure payload should have the field coordinate information
    And the first Configure JSON payload contains pointing.groups[0].field
    And the second Configure JSON payload does not contain pointing.groups[0].field
    And the third Configure JSON payload does not contain pointing.groups[0].field
    # ... and subsequent configure payloads should be partial configurations
    And the first Configure JSON payload does not contain tmc.partial_configuration
    And the second Configure JSON payload has a tmc.partial_configuration value equal to True
    And the third Configure JSON payload has a tmc.partial_configuration value equal to True
    # and this bit tests that the mosaic tiles are where they should be
    And the first Configure JSON payload has a pointing.groups[0].trajectory.attrs.x value equal to -1.0
    And the first Configure JSON payload has a pointing.groups[0].trajectory.attrs.y value equal to 1.0
    And the second Configure JSON payload has a pointing.groups[0].trajectory.attrs.x value equal to -5.0
    And the second Configure JSON payload has a pointing.groups[0].trajectory.attrs.y value equal to 5.0
    And the third Configure JSON payload has a pointing.groups[0].trajectory.attrs.x value equal to -2.0
    And the third Configure JSON payload has a pointing.groups[0].trajectory.attrs.y value equal to 2.0

