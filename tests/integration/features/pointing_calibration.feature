Feature: Run scripts for various scheduling blocks.

  @XTP-57899 @XTP-57209 @XTP-57212
  Scenario Outline: Run an observing script on a scheduling block.
    Given a telescope control system for <telescope>
    And EMPTY subarray 1
    When I run <script> on subarray 1 with SBD <sbd>
    Then the subarray obsState passes through CONFIGURING <num_scans_expected> times
    And the subarray obsState passes through SCANNING <num_scans_expected> times
    And the final obsState is EMPTY

  Examples:
    | script                     | sbd                         | telescope | num_scans_expected |
    | allocate_and_observe_sb    | testfile_SS120_sb_mid.json  | ska_mid   | 7                  |
    | allocate_and_observe_sb    | testfile_imaging_sb_low.json| ska_low   | 1                  |

  @XTP-58175 @XTP-57209 @XTP-57212
  Scenario Outline: Reset, update, then maintain the pointing model
    Given a telescope control system for <telescope>
    And EMPTY subarray 1
    When I run <script> on subarray 1 with SBD <sbd>

    # First scan is a pointing calibration in a new part of the sky, so RESETs the pointing model
    Then the first Configure JSON payload has a pointing.correction value equal to RESET
    # Also confirm that pointing correction was not in any of the partial configurations of the 5-point
    And the second Configure JSON payload does not contain pointing.correction
    And the third Configure JSON payload does not contain pointing.correction
    And the fourth Configure JSON payload does not contain pointing.correction
    And the fifth Configure JSON payload does not contain pointing.correction

    # Subsequent science scan (#6) UPDATEs the pointing correction
    And the sixth Configure JSON payload has a pointing.correction value equal to UPDATE

    # Next science scan (#7) MAINTAINs the pointing correction. While MAINTAIN is an
    # acceptable enumeration value for pointing model correction and could be relayed
    # to TMC, for backwards compatibility in PI23 MAINTAIN should be omitted from the
    # CDM payload
    And the seventh Configure JSON payload does not contain pointing.correction

    Examples:
    | script                     | sbd                        | telescope |
    | allocate_and_observe_sb    | testfile_SS120_sb_mid.json | ska_mid   |