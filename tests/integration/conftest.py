import logging
import queue
from collections import Counter
from contextlib import contextmanager
from dataclasses import dataclass
from pathlib import Path
from typing import Optional

import pytest
from ska_control_model import ObsState
from ska_ser_logging import configure_logging
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import EventType

from ska_oso_scripting.functions import sb

configure_logging(logging.DEBUG)
LOGGER = logging.getLogger(__name__)


@pytest.fixture
def constants():
    """Fixture for storing constants used in tests"""
    project_root = Path(__file__).parents[2]
    return {
        "PROJECT_ROOT": project_root,
        "DATA_DIR": project_root / "tests/resources/integration_test_cases",
    }


@pytest.fixture
def create_sbd_from_json_file(constants):
    """Fixture for generating SBD from JSON File"""

    def _semantically_invalid_sbd(invalid_sbd_json):
        invalid_sbd_json = invalid_sbd_json
        sbd = sb.load_sbd(constants["DATA_DIR"] / invalid_sbd_json)
        return sbd

    return _semantically_invalid_sbd


@dataclass
class EventsSummary:
    obsstate_counts: Counter[ObsState, int]
    """Counts how many times the device went through each obsState"""

    final_obsstate: Optional[ObsState]
    """Final device obsState at the end of the test"""

    def __init__(self, events: MockTangoEventCallbackGroup):
        """
        Create an EventsSummary from an ska-tango-testing MockTangoEventCallbackGroup.
        """
        self.obsstate_counts = Counter()
        self._summarise(events)

    def _summarise(self, events: MockTangoEventCallbackGroup):
        # TODO: write a consumer that will generate the desired events summary without inspecting private attrs
        q: queue.SimpleQueue = events["obsState"]._callable._call_queue
        final_obsstate = None
        while not q.empty():
            try:
                _, event_data, _ = q.get_nowait()
                # More brittle attribute access, although Tango EventData structure should be almost static
                event_value = event_data[0].attr_value.value
                obsstate = ObsState(event_value)
                self.obsstate_counts[obsstate] += 1
                final_obsstate = obsstate
            except queue.Empty:
                pass
        self.final_obsstate = final_obsstate


@contextmanager
def event_recorder(context, san_fqdn):
    change_event_callbacks = MockTangoEventCallbackGroup("obsState")
    san_proxy = context.get_device(san_fqdn)
    sid = san_proxy.subscribe_event(
        "obsState",
        EventType.CHANGE_EVENT,
        change_event_callbacks["obsState"],
    )
    yield change_event_callbacks
    san_proxy.unsubscribe_event(sid)
