Changelog
=========

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

Unreleased
***********

11.0.0
***********
* Remove use of `number_of_channels` from the MCCS part of the SBD, instead get the information from the CSP configuration
* Added support for GalacticCoordinates, ICRSCoordinates, AltAzCoordinates and SpecialCoordinates in support of ADR-63
* Added support for LOW Galactic tied array observations
* Added support for LOW observations with multiple spectral windows
* Added a workaround to temporarily disable sending pointing groups to TMC as these are not yet supported
* Added partial support for Pointed Mosaic observations
* Moved event topics to be emitted by scripting library functions during execution from OET to the scripting library
* Added PyTango and PyPubSub dependencies
* Removed OET dependency

10.4.0
******
* Fixed issue where `scripts` folder was not included in the `ska-oso-scripting` wheel
* Created a custom Exception class (`SkuidRuntimeError`) for SKUID related exceptions raised by `ska-oso-scripting` functions
* Added `abort.py` script to `scripts` folder as a static script
* Added `MANIFEST.skao.int` to `pyproject.toml` list of included files

10.3.0
******
* Added pointing groups in support of `ska-tmc-configure/4.1`

10.2.1
******
* Extended the workarounds to include those found during the Mid SBDefinition execution
* Removed use of `SCAN_ID_GENERATOR` in the `allocate_and_observe_sb.py` script

10.2.0
******
* Updated to PDM v17.0.1, CDM v12.6.0, ska-tango-base v1.2.3
* Removed explicit dependency on connexion
* Fixed Bug where RuntimeError was not correctly thrown if EB_ID environment variable not set
* Refactored `pdm_transforms.sdp` module into two separate modules: `create_sdp` and `convert_sdp`
* Updated `create_sdpconfiguration_centralnode()` to have processing block parameter requested by AIV
* Updated `create_sdpconfiguration_centralnode()` to use vis-receive version 4.5.0
* Added support for 5-point targets in `create_sdp` module 
* Removed `convert_frequenct_band()` from `pdm_transforms.dish`
* Refactored and renamed `convert_dish_configuration` to `create_dish_configuration` in `pdm_transforms.dish`
* Refactored `convert_commonconfiguration` in `pdm_transform.csp`
* Updated to use `ska-csp-configurescan/4.1` (change to the interface argument only)
* Fixed bug with calculation of start frequency of CSP processing region 
* Number of CSP FSPs is calculated per correlation spw using CSP's own algorithms
* Fixed bug where multiple correlation spectral windows would claim the same FSP IDs 
* Get the band_5_tuning from the midcbf part of the SBDefinition rather than the field in the common
* Removed use of `SCAN_ID_GENERATOR` from `ska-oso-oet` and use `SkuidClient` directly instead
* Added generation of SDP configuration data for Low PST observations

10.1.1
******
* Updated `convert_commonconfiguration()` in `pdm_transforms.csp` to include the eb_id kwarg
* Updated MID_CHANNEL_WIDTH and LOW_CHANNEL_WIDTH astropy Quantities in `pdm_transforms.csp` module
* Fixed Bug where LOW bandwidth was being reported in MHz rather than Hz
* Handle lowcbf common being None when mapping to Configure
* Set sbi_ids in SDP assign resources to an empty list, until the proper SBI lifecycle is implemented

10.1.0
******
* Added `create_sdpconfiguration_centralnode()` and `create_sdpconfiguration_subarraynode()` to `pdm_transforms.sdp` module

10.0.0
******
* BREAKING CHANGE: Removed support for `ska-tmc-configure/2.3` schema
* Added support for `ska-tmc-configure/4.0` in support of ADR-99
* Updated to CDM version ^12.5.1

9.1.0
*****
* Adds conversion of SKA-Low PST beam coordinates to the equivalent ADR-63 sky coordinates
* Updated to ODA v6.0.0 and OET v6.3.0
* Default value of `functions.devicecontrol.telescope_control.telescope_off()` function changed from `STANDBY` to `OFF`
* Exposed expected final state as optional parameter in `off()` method of `objects.Telescope` object
* Added scan-configuration parameters required by PST

9.0.3
*****
* Updated to CDMv12.0.0
* Removed hardcoded interface values for sdp-assignres and sdp-configure
* Added support for a Low PST observations

9.0.2
*****
* Unpin ODA dependency to allow minor and patch updates

9.0.1
*****
* Changelog fix

9.0.0
*****

* BREAKING CHANGE: Unified mid and low scripts as a single `scripts/allocate_and_observe_sb.py`
* BREAKING CHANGE: function `create_cdm_assign_resources_request_from_scheduling_block()` now returns a single instance
* Updates to CDM version 11.0.1
* Updates to PDM version 15.0.0
* Updates to ODA version 5.3.0
* Adds support for `ska-low-tmc-assignresources/4.0`
* Adds support for `ska-low-tmc-configure/4.0`

8.1.0
*****
* Updates to CDM version 10.
* Updates to PDM version 14.
* Adds support for non-sidereal targets, (reference_frame="special")

8.0.0
*****

* BREAKING CHANGE: updated to use OSO PDM v12.0.1 and TMC CDM v9.1.1
* Added `convert_pointingcorrection()` to `pdm_transforms.dish` module in support of SS-120
* BREAKING CHANGE: Removed support for MCCS
